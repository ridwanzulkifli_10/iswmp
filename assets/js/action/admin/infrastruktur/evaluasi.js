$(function () {
	console.log("You are running jQuery version: " + $.fn.jquery);
	$(".summernote").summernote({
		height: 200, //set editable area's height
		codemirror: {
			// codemirror options
			theme: "monokai",
		},
		toolbar: {},
		disableResizeEditor: true,
		focus: true,
	});

	$(".note-editable").attr("name", "profile-input");

	$(".select2").select2();
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	$(".datatable").DataTable();

	//Date picker
	$("#reservationdate").datetimepicker({
		format: "L",
	});

	window.img = "";
	$("input[data-bootstrap-switch]").each(function () {
		// $(this).bootstrapSwitch('state', $(this).prop('checked'));
		$(this).bootstrapSwitch({
			onSwitchChange: function (e, state) {
				st = state;
			},
		});
	});

	$("#modal-default").on("show.bs.modal", function () {});

	$("#pasca > a").attr("class", "nav-link active");
	$("#pasca").attr("class", "nav-item menu-is-opening menu-open");
	$("#evaluasi > a").attr("class", "nav-link active");
	$("#evaluasi > a > i").addClass("text-info");

	$("#add-data-1").on("click", function () {
		$("#modal-1").modal({
			show: true,
		});
		$("#tpst_1").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_1");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_1").html(elem).trigger("change");
		$('[name="relevansi-input"]').val(0).trigger("change");
		$("#id_1").val("");
	});

	$("#add-data-2").on("click", function () {
		$("#modal-2").modal({
			show: true,
		});
		$("#tpst_2").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_2");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_2").html(elem).trigger("change");
		$('[name="efisiensi-input"]').val(0).trigger("change");
		$("#id_2").val("");
	});

	$("#add-data-3").on("click", function () {
		$("#modal-3").modal({
			show: true,
		});
		$("#tpst_3").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_3");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_3").html(elem).trigger("change");
		$('[name="efektifitas-input"]').val(0).trigger("change");
		$("#id_3").val("");
	});

	$("#add-data-4").on("click", function () {
		$("#modal-4").modal({
			show: true,
		});
		$("#tpst_4").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_4");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_4").html(elem).trigger("change");
		$('[name="dampak-input"]').val(0).trigger("change");
		$("#id_4").val("");
	});

	$("#save-data-1").on("click", function () {
		savedata("data_relevansi");
	});

	$("#save-data-2").on("click", function () {
		savedata("data_efisiensi");
	});

	$("#save-data-3").on("click", function () {
		savedata("data_efektifitas");
	});

	$("#save-data-4").on("click", function () {
		savedata("data_dampak");
	});

	loaddata("data_relevansi");
	loadoption("data_relevansi", 1);
	$("#custom-2").on("click", function () {
		loaddata("data_efisiensi");
		loadoption("data_efisiensi", 2);
	});
	$("#custom-3").on("click", function () {
		loaddata("data_efektifitas");
		loadoption("data_efektifitas", 3);
	});
	$("#custom-4").on("click", function () {
		loaddata("data_dampak");
		loadoption("data_dampak", 4);
	});
});

function loadoption(param, kode) {
	$.ajax({
		type: "post",
		dataType: "json",
		url: "gettpst",
		data: {
			param: param,
		},
		success: function (result) {
			var res = result.data;
			var code = result.code;
			var item = code == 1 ? JSON.stringify(res) : [];

			window.localStorage.setItem("datatpst_" + kode, item);
			var elem = '<option value="">...</option>';
			if (result.code == 1) {
				for (let index = 0; index < res.length; index++) {
					elem += `<option value="${res[index]["id"]}">${res[index]["tpst"]}</option>`;
				}
			}

			$("#tpst_" + kode).html(elem);
			$("#tpst_" + kode).trigger("change");
		},
	});
}

function loaddata(param) {
	$.ajax({
		type: "post",
		dataType: "json",
		url: "getcriteria",
		data: {
			param: param,
		},
		success: function (result) {
			if (result.code == 1) {
				if (param == "data_relevansi") {
					var dt = $("#list_1").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "ssk" },
							{ mDataProp: "masterplan" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Kegiatan sudah tercantum dalam SSK terupdate 5 tahun terakhir",
										"Kegiatan sudah tercantum dalam SSK tapi belum dimutakhirkan dalam 5 tahun terakhir",
										"Kegiatan tidak tercantum dalam SSK",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Kegiatan sudah tercantum dalam Masterplan atau PTMP terupdate 5 tahun terakhir",
										"Kegiatan sudah tercantum dalam Masterplan atau PTMP tapi belum direview dalam 5 tahun terakhir",
										"Belum ada dokumen Masterplan atau PTMP",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editrelevansi(${row.id},'${row.tpst}','${row.tpst_desc}','${row.ssk}', '${row.masterplan}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_relevansi')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [4],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_efisiensi") {
					var dt = $("#list_2").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "waktu" },
							{ mDataProp: "biaya" },
							{ mDataProp: "ruanglingkup" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"100% atau kurang dari rencana waktu pelaksanaan semula",
										"Lebih dari 100%, tetapi kurang 150% dari rencana waktu pelaksanaan semula",
										"Lebih 150% dari waktu rencana pelaksanaan semula",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"100% atau kurang dari rencana biaya semula",
										"Lebih dari 100% dan kurang 110% dari rencana biaya semula",
										"Lebih 110% dari rencana biaya semula",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Seluruh ruang lingkup pekerjaan terpenuhi atau terbangun",
										"Seluruh ruang lingkup pekerjaan terpenuhi tetapi ada perubahan (penambahan atau pengurangan pekerjaan)",
										"Ada ruang lingkup yang tidak terpenuhi atau tidak terbangun",
									];

									return $rowData[data];
								},
								aTargets: [4],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editefisiensi('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.waktu}', '${row.biaya}', '${row.ruanglingkup}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_efisiensi')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [5],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_efektifitas") {
					var dt = $("#list_3").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "efektifitas" },
							{ mDataProp: "manfaat" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Bangunan digunakan dan bermanfaat sesuai perencanaan",
										"Terdapat beberapa komponen bangunan yang belum digunakan/termanfaatkan sesuai perencanaan",
										"Bangunan tidak atau belum termanfaatkan sesuai perencanaan",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Realisasi jiwa terlayani 75-100% terhadap target perencanaan",
										"Realisasi jiwa terlayani 50-75% terhadap target perencanaan",
										"Realisasi jiwa terlayani <50% terhadap target perencanaan",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editefektifitas('${row.id}', '${row.tpst}','${row.tpst_desc}','${row.efektifitas}', '${row.manfaat}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_efektifitas')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [4],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_dampak") {
					var dt = $("#list_4").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "perlindungan" },
							{ mDataProp: "peningkatan" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Hasil pemantauan kualitas lingkungan (air permukaan, dll) dibawah baku mutu",
										"Hasil pemantauan kualitas lingkungan (air permukaan, dll) melebihi baku mutu",
										"Tidak melakukan pemantauan kualitas lingkungan (air permukaan, dll)",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Terdapat peningkatan pendapatan rata-rata masyarakat sekitar TPST dan penduduk yang bekerja",
										"Tidak ada peningkatan pendapatan rata-rata masyarakat sekitar TPST",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editdampak('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.perlindungan}', '${row.peningkatan}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_dampak')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [4],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}
			} else {
				var table = $("#list_1").DataTable();
				table.clear().draw();
			}
		},
	});
}

function savedata(param) {
	var formData = new FormData();
	if (param == "data_relevansi") {
		var relevansi = $('[name="relevansi-input"]');
		for (let i = 0; i < relevansi.length; i++) {
			var elem = relevansi[i];
			if (elem.id == "tpst_1") {
				elem.id = elem.id.replace("_1", "");
				formData.append("tpst_desc", $("#select2-tpst_1-container").text());
			}
			formData.append(elem.id, elem.value);
		}

		formData.append("table", param);
		if ($("#id_1").val()) {
			formData.append("id", $("#id_1").val());
		}
		if ($("#id_1").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Relevansi";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Relevansi";
		}
	}

	if (param == "data_efisiensi") {
		var efisiensi = $('[name="efisiensi-input"]');
		for (let i = 0; i < efisiensi.length; i++) {
			var elem = efisiensi[i];
			if (elem.id == "tpst_2") {
				elem.id = elem.id.replace("_2", "");
				formData.append("tpst_desc", $("#select2-tpst_2-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_2").val()) {
			formData.append("id", $("#id_2").val());
		}
		if ($("#id_2").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Efisiensi ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Efisiensi ";
		}
	}

	if (param == "data_efektifitas") {
		var efektifitas = $('[name="efektifitas-input"]');
		for (let i = 0; i < efektifitas.length; i++) {
			var elem = efektifitas[i];
			if (elem.id == "tpst_3") {
				elem.id = elem.id.replace("_3", "");
				formData.append("tpst_desc", $("#select2-tpst_3-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_3").val()) {
			formData.append("id", $("#id_3").val());
		}
		if ($("#id_3").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Efektifitas";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Efektifitas";
		}
	}

	if (param == "data_dampak") {
		var dampak = $('[name="dampak-input"]');
		for (let i = 0; i < dampak.length; i++) {
			var elem = dampak[i];
			if (elem.id == "tpst_4") {
				elem.id = elem.id.replace("_4", "");
				formData.append("tpst_desc", $("#select2-tpst_4-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_4").val()) {
			formData.append("id", $("#id_4").val());
		}
		if ($("#id_4").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Dampak";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Dampak";
		}
	}

	$.ajax({
		type: "post",
		url: baseurl,
		dataType: "json",
		cache: false,
		contentType: false,
		processData: false,
		data: formData,
		async: false,
		success: function (result) {
			Swal.fire({
				title: "Sukses!",
				text: msg,
				icon: "success",
				showConfirmButton: false,
				timer: 1500,
			});

			$(".modal").modal("hide");
			var kode = [
				"-",
				"data_relevansi",
				"data_efisiensi",
				"data_efektifitas",
				"data_dampak",
			].indexOf(param);
			$('[data-select2-id="tpst_' + kode + '"]').attr("id", "tpst_" + kode);
			$("#custom-" + kode).trigger("click");
			// loaddata(param);
		},
	});
}

function editrelevansi(id, tpst, tpst_desc, ssk, masterplan) {
	$("#add-data-1").trigger("click");
	$("#tpst_1").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_1").prop(`disabled`, true);
	$("#id_1").val(id);
	$("#ssk").val(ssk).trigger("change");
	$("#masterplan").val(masterplan).trigger("change");
}

function editefisiensi(id, tpst, tpst_desc, waktu, biaya, ruanglingkup) {
	$("#add-data-2").trigger("click");
	$("#tpst_2").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_2").prop(`disabled`, true);
	$("#id_2").val(id);
	$("#waktu").val(waktu).trigger("change");
	$("#biaya").val(biaya).trigger("change");
	$("#ruanglingkup").val(ruanglingkup).trigger("change");
}

function editefektifitas(id, tpst, tpst_desc, efektifitas, manfaat) {
	$("#add-data-3").trigger("click");
	$("#tpst_3").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_3").prop(`disabled`, true);
	$("#id_3").val(id);
	$("#efektifitas").val(efektifitas).trigger("change");
	$("#manfaat").val(manfaat).trigger("change");
}
function editdampak(id, tpst, tpst_desc, perlindungan, peningkatan) {
	$("#add-data-4").trigger("click");
	$("#tpst_4").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_4").prop(`disabled`, true);
	$("#id_4").val(id);
	$("#perlindungan").val(perlindungan).trigger("change");
	$("#peningkatan").val(peningkatan).trigger("change");
}

function deleteData(id, table) {
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: "btn btn-success btn-sm swal2-styled-custom",
			cancelButton: "btn btn-danger btn-sm swal2-styled-custom",
		},
		buttonsStyling: false,
	});

	swalWithBootstrapButtons
		.fire({
			title: "Anda Yakin, hapus data ini?",
			text: "",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: '<i class="fas fa-check"></i> Ya',
			cancelButtonText: '<i class="fas fa-times"></i> Tidak',
			reverseButtons: true,
		})
		.then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					type: "post",
					dataType: "json",
					url: "deletecriteria",
					data: {
						id: id,
						table: table,
					},
					success: function (data) {
						Swal.fire({
							title: "Sukses!",
							text: "Hapus Data",
							icon: "success",
							showConfirmButton: false,
							timer: 1500,
						});
						loaddata(table);
					},
				});
			}
		});
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		var blah = input.id.replace("image", "blah");
		reader.onload = function (e) {
			$("#" + blah).attr("src", e.target.result);
			window.img = e.target.result;
		};
		reader.readAsDataURL(input.files[0]); // convert to base64 string
	}
}

function modaldetail(id, username, role, status, name, foto) {
	$("#modal-detail").modal({
		show: true,
	});

	$(".modal-title").html("Detail");

	var stt = "";
	if (status == 1) {
		stt += `<span class="badge badge-primary right">Aktif</span>`;
	} else {
		stt += `<span class="badge badge-warning right">Non Aktif</span>`;
	}

	$("#detail-foto").attr("src", foto);
	$("#detail-name").text(name);
	$("#detail-username").html("username: <i>" + username + "</i>");
	$("#detail-status").html(stt);
	$("#detail-role").text(role);
}
