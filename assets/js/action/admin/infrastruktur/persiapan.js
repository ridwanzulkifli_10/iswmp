$(function () {
	console.log("You are running jQuery version: " + $.fn.jquery);
	$(".summernote").summernote({
		height: 200, //set editable area's height
		codemirror: {
			// codemirror options
			theme: "monokai",
		},
		toolbar: {},
		disableResizeEditor: true,
		focus: true,
	});

	$(".note-editable").attr("name", "profile-input");

	$(".select2").select2();
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	$(".datatable").DataTable();

	//Date picker
	$("#reservationdate").datetimepicker({
		format: "L",
	});

	window.img = "";
	$("input[data-bootstrap-switch]").each(function () {
		// $(this).bootstrapSwitch('state', $(this).prop('checked'));
		$(this).bootstrapSwitch({
			onSwitchChange: function (e, state) {
				st = state;
			},
		});
	});

	// $("#save-data-1").attr("disabled", true);
	$("#modal-default").on("show.bs.modal", function () {});

	$("#pelaksana > a").attr("class", "nav-link active");
	$("#pelaksana").attr("class", "nav-item menu-is-opening menu-open");
	$("#persiapan > a").attr("class", "nav-link active");
	$("#persiapan > a > i").addClass("text-info");

	$("#skema").change(function () {
		if ($("#skema").val() == 3) {
			$("#div-skema").show();
		} else {
			$("#div-skema").hide();
			$("#skem2").val("");
		}
	});

	$("#pengadaan_tender").change(function () {
		if ($("#pengadaan_tender").val() == 3) {
			$("#div-pengadaan").show();
		} else {
			$("#div-pengadaan").hide();
			$("#pengadaan_tender2").val("");
		}
	});

	$("#add-data-1").on("click", function () {
		$("#modal-1").modal({
			show: true,
		});

		$("#div-skema").hide();
		$('[name="kegiatan-input"]').val("");
		$("#tpst_1").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_1");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_1").html(elem).trigger("change");
		$("#dana").val(0).trigger("change");
		$("#skema").val(0).trigger("change");
		$("#kontrak").val(0).trigger("change");
		$(".summernote").summernote("reset");
		$("#id_1").val("");
	});

	$("#add-data-2").on("click", function () {
		$("#modal-2").modal({
			show: true,
		});
		$("#tpst_2").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_2");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_2").html(elem).trigger("change");

		$("#stat_anggaran").val(0).trigger("change");
		$("#id_2").val("");
	});

	$("#add-data-3").on("click", function () {
		$("#modal-3").modal({
			show: true,
		});
		$("#tpst_3").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_3");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_3").html(elem).trigger("change");

		$("#stat_kak").val(0).trigger("change");
		$("#id_3").val("");
	});

	$("#add-data-4").on("click", function () {
		$("#modal-4").modal({
			show: true,
		});
		$("#tpst_4").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_4");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_4").html(elem).trigger("change");

		$("#stat_pagu").val(0).trigger("change");
		$("#id_4").val("");
	});

	$("#add-data-5").on("click", function () {
		$("#modal-5").modal({
			show: true,
		});
		$("#tpst_5").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_5");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_5").html(elem).trigger("change");

		$("#stat_seleksi").val(0).trigger("change");
		$("#id_5").val("");
	});

	$("#add-data-6").on("click", function () {
		$("#modal-6").modal({
			show: true,
		});
		$("#tpst_6").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_6");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_6").html(elem).trigger("change");

		$("#stat_wb").val(0).trigger("change");
		$("#id_6").val("");
	});

	$("#add-data-7").on("click", function () {
		$("#modal-7").modal({
			show: true,
		});

		$("#div-pengadaan").hide();
		$('[name="tender-input"]').val("");
		$("#tpst_7").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_7");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_7").html(elem).trigger("change");
		$("#dana_tender").val(0).trigger("change");
		$("#pengadaan_tender").val(0).trigger("change");
		$("#id_7").val("");
	});

	$("#add-data-8").on("click", function () {
		$("#modal-8").modal({
			show: true,
		});
		$("#tpst_8").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_8");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_8").html(elem).trigger("change");
		$('[name="konstruksi-input[]"]').val("");
		$('[name="dokumen-input[]"]').val("");
		$('[name="penjelasan-input[]"]').val("");
		$('[name="penawaran-input[]"]').val("");
		$('[name="pembukaan-input[]"]').val("");
		$('[name="kualifikasi-input[]"]').val("");
		$('[name="pengumuman-input[]"]').val("");
		$('[name="sanggah-input[]"]').val("");
		$('[name="negosiasi-input[]"]').val("");
		$('[name="penetapan-input[]"]').val("");
		$('[name="pemenang-input[]"]').val("");
		$('[name="penunjukan-input[]"]').val("");
		$('[name="penandatanganan-input[]"]').val("");
		$("#id_8").val("");
	});

	$("#add-data-9").on("click", function () {
		$("#modal-9").modal({
			show: true,
		});
		$("#tpst_9").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_9");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_9").html(elem).trigger("change");

		$("#stat_pelaksanaan").val(0).trigger("change");
		$("#id_9").val("");
	});

	$("#save-data-1").on("click", function () {
		savedata("data_nama_kegiatan");
	});

	$("#save-data-2").on("click", function () {
		savedata("data_penganggaran");
	});

	$("#save-data-3").on("click", function () {
		savedata("data_kak");
	});

	$("#save-data-4").on("click", function () {
		savedata("data_pagu");
	});

	$("#save-data-5").on("click", function () {
		savedata("data_seleksi");
	});

	$("#save-data-6").on("click", function () {
		savedata("data_wb");
	});

	$("#save-data-7").on("click", function () {
		savedata("data_profil_tender");
	});

	$("#save-data-8").on("click", function () {
		savedata("data_proses_barang_jasa");
	});

	$("#save-data-9").on("click", function () {
		savedata("data_stat_pelaksanaan_barang_jasa");
	});

	loaddata("data_nama_kegiatan");
	loadoption("data_nama_kegiatan", 1);

	$("#custom-1").on("click", function () {
		loaddata("data_nama_kegiatan");
		loadoption("data_nama_kegiatan", 1);
	});

	$("#custom-2").on("click", function () {
		loaddata("data_penganggaran");
		loadoption("data_penganggaran", 2);
	});

	$("#custom-3").on("click", function () {
		loaddata("data_kak");
		loadoption("data_kak", 3);
	});

	$("#custom-4").on("click", function () {
		loaddata("data_pagu");
		loadoption("data_pagu", 4);
	});

	$("#custom-5").on("click", function () {
		loaddata("data_seleksi");
		loadoption("data_seleksi", 5);
	});

	$("#custom-6").on("click", function () {
		loaddata("data_wb");
		loadoption("data_wb", 6);
	});

	$("#custom-7").on("click", function () {
		loaddata("data_profil_tender");
		loadoption("data_profil_tender", 7);
	});

	$("#custom-8").on("click", function () {
		loaddata("data_proses_barang_jasa");
		loadoption("data_proses_barang_jasa", 8);
	});

	$("#custom-9").on("click", function () {
		loaddata("data_stat_pelaksanaan_barang_jasa");
		loadoption("data_stat_pelaksanaan_barang_jasa", 9);
	});
});

function loadoption(param, kode) {
	$.ajax({
		type: "post",
		dataType: "json",
		url: "gettpst",
		data: {
			param: param,
		},
		success: function (result) {
			var res = result.data;
			var code = result.code;
			var item = code == 1 ? JSON.stringify(res) : [];

			window.localStorage.setItem("datatpst_" + kode, item);
			var elem = '<option value="">...</option>';
			if (result.code == 1) {
				for (let index = 0; index < res.length; index++) {
					elem += `<option value="${res[index]["id"]}">${res[index]["tpst"]}</option>`;
				}
			}

			$("#tpst_" + kode).html(elem);
			$("#tpst_" + kode).trigger("change");
		},
	});
}

function loaddata(param) {
	$.ajax({
		type: "post",
		dataType: "json",
		url: "getcriteria",
		data: {
			param: param,
		},
		success: function (result) {
			if (result.code == 1) {
				if (param == "data_nama_kegiatan") {
					var dt = $("#list_1").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "tahun" },
							{ mDataProp: "paket" },
							{ mDataProp: "dana" },
							{ mDataProp: "lokasi" },
							{ mDataProp: "skema" },
							{ mDataProp: "skema2" },
							{ mDataProp: "kontrak" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "APBN MURNI", "APBN LOAN"];

									return $rowData[row.dana];
								},
								aTargets: [4],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Full Loan",
										"Ada kerjasama dengan badan usaha",
										"Lainnya",
									];

									return $rowData[row.skema];
								},
								aTargets: [6],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									if (row.skema2 == "") {
										$rowData = "-";
									} else {
										$rowData = row.skema2;
									}

									return $rowData;
								},
								aTargets: [7],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Kontrak payung",
										"Perjanjian Kerja Sama",
									];

									return $rowData[row.kontrak];
								},
								aTargets: [8],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editkegiatan(${row.id},'${row.tpst}','${row.tpst_desc}','${row.tpst_desc}','${row.tahun}', '${row.paket}', '${row.dana}', '${row.lokasi}', '${row.skema}','${row.skema2}', '${row.kontrak}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_nama_kegiatan')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [9],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_penganggaran") {
					var dt = $("#list_2").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "stat_anggaran" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "Sudah", "Belum"];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editanggaran('${row.id}','${row.tpst}', '${row.tpst_desc}','${row.stat_anggaran}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_penganggaran')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [3],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_kak") {
					var dt = $("#list_3").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "stat_kak" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "Sudah", "Belum"];

									return $rowData[row.stat_kak];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editkak('${row.id}','${row.tpst}', '${row.tpst_desc}', '${row.stat_kak}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_kak')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [3],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_pagu") {
					var dt = $("#list_4").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "stat_pagu" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "Sudah", "Belum"];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editpagu('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.stat_pagu}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_pagu')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [3],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_seleksi") {
					var dt = $("#list_5").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "stat_seleksi" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "sudah", "Belum"];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editseleksi('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.stat_seleksi}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_seleksi')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [3],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_wb") {
					var dt = $("#list_6").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "stat_wb" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "Sudah", "Belum"];

									return $rowData[row.stat_wb];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editwb('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.stat_wb}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_wb')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [3],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_profil_tender") {
					var dt = $("#list_7").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "tahun_tender" },
							{ mDataProp: "paket_tender" },
							{ mDataProp: "dana_tender" },
							{ mDataProp: "klpd_tender" },
							{ mDataProp: "satuan_tender" },
							{ mDataProp: "pengadaan_tender" },
							{ mDataProp: "pengadaan_tender2" },
							{ mDataProp: "nilai_tender" },
							{ mDataProp: "lokasi_tender" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "APBN MURNI", "APBN LOAN"];

									return $rowData[row.dana_tender];
								},
								aTargets: [4],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Pekerjaan Konstruksi",
										"Jasa Konsultansi Badan Usaha",
										"lainnya",
									];

									return $rowData[row.pengadaan_tender];
								},
								aTargets: [7],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									if (row.pengadaan_tender2 == "") {
										$rowData = "-";
									} else {
										$rowData = row.pengadaan_tender2;
									}

									return $rowData;
								},
								aTargets: [8],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="edittender('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.tahun_tender}', '${row.paket_tender}', '${row.dana_tender}', '${row.klpd_tender}', '${row.satuan_tender}', '${row.pengadaan_tender}','${row.pengadaan_tender2}', '${row.nilai_tender}', '${row.lokasi_tender}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_profil_tender')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [11],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_proses_barang_jasa") {
					var dt = $("#list_8").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "konstruksi" },
							{ mDataProp: "dokumen" },
							{ mDataProp: "penjelasan" },
							{ mDataProp: "penawaran" },
							{ mDataProp: "pembukaan" },
							{ mDataProp: "kualifikasi" },
							{ mDataProp: "pengumuman" },
							{ mDataProp: "sanggah" },
							{ mDataProp: "negosiasi" },
							{ mDataProp: "penetapan" },
							{ mDataProp: "pemenang" },
							{ mDataProp: "penunjukan" },
							{ mDataProp: "penandatanganan" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var dat = row.konstruksi.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.dokumen.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.penjelasan.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [4],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.penawaran.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [5],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.pembukaan.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [6],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.kualifikasi.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [7],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.pengumuman.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [8],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.sanggah.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [9],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.negosiasi.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [10],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.penetapan.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [11],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.pemenang.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [12],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.penunjukan.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [13],
							},
							{
								mRender: function (data, type, row) {
									var dat = row.penandatanganan.split(",");
									var $rowData =
										`<div class="card">
											<div class="card-body">
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">JADWAL RENCANA :</p>
													<p class="d-flex flex-column">
														<span class="text-muted"> ` +
										dat[0] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														JADWAL AKTUAL :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[1] +
										`</span>
													</p>
												</div>
												<div class="d-flex justify-content-between">
													<p class="text-primary text-sm">
														DEVIASI WAKTU :
													</p>
													<p class="d-flex flex-column ">
														<span class="text-muted">` +
										dat[2] +
										`</span>
													</p>
												</div>
											</div>
										</div>`;

									return $rowData;
								},
								aTargets: [14],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
				                            <div class="btn-group">
				                            <button type="button" class="btn btn-info">Action</button>
				                            <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
				                              <span class="sr-only">Toggle Dropdown</span>
				                            </button>
				                            <div class="dropdown-menu" role="menu">
				                              <a class="dropdown-item" href="#" onclick="editbarangjasa('${
																				row.id
																			}','${row.tpst}','${
										row.tpst_desc
									}', '${row.konstruksi.split(",")}', '${row.dokumen.split(
										","
									)}', '${row.penjelasan.split(",")}', '${row.penawaran.split(
										","
									)}', '${row.pembukaan.split(",")}', '${row.kualifikasi.split(
										","
									)}','${row.pengumuman.split(",")}', '${row.sanggah.split(
										","
									)}', '${row.negosiasi.split(",")}', '${row.penetapan.split(
										","
									)}', '${row.pemenang.split(",")}', '${row.penunjukan.split(
										","
									)}', '${row.penandatanganan.split(
										","
									)}')"><i class="far fa-edit"></i> Edit</a>
				                              <a class="dropdown-item" href="#" onclick="deleteData(${
																				row.id
																			}, 'data_proses_barang_jasa')"><i class="far fa-trash-alt"></i> Hapus</a>
				                            </div>
				                          </div>`;

									return $rowData;
								},
								aTargets: [15],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_stat_pelaksanaan_barang_jasa") {
					var dt = $("#list_9").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "stat_pelaksanaan" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Masih dalam jadwal",
										"perlu percepatan pelaksanaan",
										"perlu perhatian khusus",
									];

									return $rowData[row.stat_pelaksanaan];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editstatpelaksanaan('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.stat_pelaksanaan}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_stat_pelaksanaan_barang_jasa')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [3],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}
			} else {
				var table = $("#list_1").DataTable();
				table.clear().draw();
			}
		},
	});
}

function savedata(param) {
	var formData = new FormData();
	if (param == "data_nama_kegiatan") {
		var kegiatan = $('[name="kegiatan-input"]');
		for (let i = 0; i < kegiatan.length; i++) {
			var elem = kegiatan[i];
			if (elem.id == "tpst_1") {
				elem.id = elem.id.replace("_1", "");
				formData.append("tpst_desc", $("#select2-tpst_1-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_1").val()) {
			formData.append("id", $("#id_1").val());
		}
		if ($("#id_1").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Nama Kegiatan";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Nama Kegiatan";
		}
	}

	if (param == "data_penganggaran") {
		var anggaran = $('[name="anggaran-input"]');
		for (let i = 0; i < anggaran.length; i++) {
			var elem = anggaran[i];
			if (elem.id == "tpst_2") {
				elem.id = elem.id.replace("_2", "");
				formData.append("tpst_desc", $("#select2-tpst_2-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_2").val()) {
			formData.append("id", $("#id_2").val());
		}
		if ($("#id_2").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Penganggaran (DIPA)";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Penganggaran (DIPA)";
		}
	}

	if (param == "data_kak") {
		var kak = $('[name="kak-input"]');
		for (let i = 0; i < kak.length; i++) {
			var elem = kak[i];
			if (elem.id == "tpst_3") {
				elem.id = elem.id.replace("_3", "");
				formData.append("tpst_desc", $("#select2-tpst_3-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_3").val()) {
			formData.append("id", $("#id_3").val());
		}
		if ($("#id_3").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Pembuatan KAK";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Pembuatan KAK";
		}
	}

	if (param == "data_pagu") {
		var pagu = $('[name="pagu-input"]');
		for (let i = 0; i < pagu.length; i++) {
			var elem = pagu[i];
			if (elem.id == "tpst_4") {
				elem.id = elem.id.replace("_4", "");
				formData.append("tpst_desc", $("#select2-tpst_4-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_4").val()) {
			formData.append("id", $("#id_4").val());
		}
		if ($("#id_4").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Pembuatan Pagu Anggaran/RAB/HPS";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Pembuatan Pagu Anggaran/RAB/HPS";
		}
	}

	if (param == "data_seleksi") {
		var seleksi = $('[name="seleksi-input"]');
		for (let i = 0; i < seleksi.length; i++) {
			var elem = seleksi[i];
			if (elem.id == "tpst_5") {
				elem.id = elem.id.replace("_5", "");
				formData.append("tpst_desc", $("#select2-tpst_5-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_5").val()) {
			formData.append("id", $("#id_5").val());
		}
		if ($("#id_5").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Dokumen Seleksi Konsultan ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Dokumen Seleksi Konsultan ";
		}
	}

	if (param == "data_wb") {
		var wb = $('[name="wb-input"]');
		for (let i = 0; i < wb.length; i++) {
			var elem = wb[i];
			if (elem.id == "tpst_6") {
				elem.id = elem.id.replace("_6", "");
				formData.append("tpst_desc", $("#select2-tpst_6-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_6").val()) {
			formData.append("id", $("#id_6").val());
		}
		if ($("#id_6").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data NoL WB  ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data NoL WB  ";
		}
	}

	if (param == "data_profil_tender") {
		var tender = $('[name="tender-input"]');
		for (let i = 0; i < tender.length; i++) {
			var elem = tender[i];
			if (elem.id == "tpst_7") {
				elem.id = elem.id.replace("_7", "");
				formData.append("tpst_desc", $("#select2-tpst_7-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_7").val()) {
			formData.append("id", $("#id_7").val());
		}
		if ($("#id_7").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Profil Tender  ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Profil Tender  ";
		}
	}

	if (param == "data_proses_barang_jasa") {
		var proses = $('[name="proses-pengadaan-input"');
		for (let i = 0; i < proses.length; i++) {
			var elem = proses[i];
			if (elem.id == "tpst_8") {
				elem.id = elem.id.replace("_8", "");
				formData.append("tpst_desc", $("#select2-tpst_8-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		var konstruksi = $("input[name^=konstruksi-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("konstruksi", konstruksi);

		var dokumen = $("input[name^=dokumen-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("dokumen", dokumen);

		var penjelasan = $("input[name^=penjelasan-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("penjelasan", penjelasan);

		var penawaran = $("input[name^=penawaran-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("penawaran", penawaran);

		var pembukaan = $("input[name^=pembukaan-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("pembukaan", pembukaan);

		var kualifikasi = $("input[name^=kualifikasi-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("kualifikasi", kualifikasi);

		var pengumuman = $("input[name^=pengumuman-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("pengumuman", pengumuman);

		var sanggah = $("input[name^=sanggah-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("sanggah", sanggah);

		var negosiasi = $("input[name^=negosiasi-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("negosiasi", negosiasi);

		var penetapan = $("input[name^=penetapan-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("penetapan", penetapan);

		var pemenang = $("input[name^=pemenang-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("pemenang", pemenang);

		var penunjukan = $("input[name^=penunjukan-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("penunjukan", penunjukan);

		var penandatanganan = $("input[name^=penandatanganan-input]")
			.map(function (idx, elem) {
				return $(elem).val();
			})
			.get()
			.join(",");
		formData.append("penandatanganan", penandatanganan);

		formData.append("table", param);

		if ($("#id_8").val()) {
			formData.append("id", $("#id_8").val());
		}
		if ($("#id_8").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Proses Pengadaan Barang/Jasa";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Proses Pengadaan Barang/Jasa";
		}
	}

	if (param == "data_stat_pelaksanaan_barang_jasa") {
		var stat_pelaksanaan = $('[name="stat-pelaksanaan-input"]');
		for (let i = 0; i < stat_pelaksanaan.length; i++) {
			var elem = stat_pelaksanaan[i];
			if (elem.id == "tpst_9") {
				elem.id = elem.id.replace("_9", "");
				formData.append("tpst_desc", $("#select2-tpst_9-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_9").val()) {
			formData.append("id", $("#id_9").val());
		}
		if ($("#id_9").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Status Pelaksanaan Pengadaan Barang/Jasa  ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Status Pelaksanaan Pengadaan Barang/Jasa  ";
		}
	}

	$.ajax({
		type: "post",
		url: baseurl,
		dataType: "json",
		cache: false,
		contentType: false,
		processData: false,
		data: formData,
		async: false,
		success: function (result) {
			Swal.fire({
				title: "Sukses!",
				text: msg,
				icon: "success",
				showConfirmButton: false,
				timer: 1500,
			});

			$(".modal").modal("hide");
			var kode = [
				"-",
				"data_nama_kegiatan",
				"data_penganggaran",
				"data_kak",
				"data_pagu",
				"data_seleksi",
				"data_wb",
				"data_profil_tender",
				"data_proses_barang_jasa",
				"data_stat_pelaksanaan_barang_jasa",
			].indexOf(param);
			$('[data-select2-id="tpst_' + kode + '"]').attr("id", "tpst_" + kode);
			$("#custom-" + kode).trigger("click");
			// loaddata(param);
		},
	});
}

function editkegiatan(
	id,
	tpst,
	tpst_desc,
	tahun,
	paket,
	dana,
	lokasi,
	skema,
	skema2,
	kontrak
) {
	$("#add-data-1").trigger("click");
	$("#tpst_1").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_1").prop(`disabled`, true);
	$("#id_1").val(id);
	$("#tahun").val(tahun);
	$("#paket").val(paket);
	$("#dana").val(dana).trigger("change");
	$("#lokasi").val(lokasi);
	if (skema == "3") {
		$("#skema").val(3).trigger("change");
		$("#skema2").val(skema2);
	} else {
		$("#skema").val(skema).trigger("change");
	}
	$("#kontrak").val(kontrak).trigger("change");
}

function editanggaran(id, tpst, tpst_desc, stat_anggaran) {
	$("#add-data-2").trigger("click");
	$("#tpst_2").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_2").prop("disabled", true);
	$("#id_2").val(id);
	$("#stat_anggaran").val(stat_anggaran).trigger("change");
}

function editkak(id, tpst, tpst_desc, stat_kak) {
	$("#add-data-3").trigger("click");
	$("#tpst_3").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_3").prop("disabled", true);
	$("#id_3").val(id);
	$("#stat_kak").val(stat_kak).trigger("change");
}

function editpagu(id, tpst, tpst_desc, stat_pagu) {
	$("#add-data-4").trigger("click");
	$("#tpst_4").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_4").prop("disabled", true);
	$("#id_4").val(id);
	$("#stat_pagu").val(stat_pagu).trigger("change");
}

function editseleksi(id, tpst, tpst_desc, stat_seleksi) {
	$("#add-data-5").trigger("click");
	$("#tpst_5").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_5").prop("disabled", true);
	$("#id_5").val(id);
	$("#stat_seleksi").val(stat_seleksi).trigger("change");
}

function editwb(id, tpst, tpst_desc, stat_wb) {
	$("#add-data-6").trigger("click");
	$("#tpst_6").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_6").prop("disabled", true);
	$("#id_6").val(id);
	$("#stat_wb").val(stat_wb).trigger("change");
}

function edittender(
	id,
	tpst,
	tpst_desc,
	tahunt,
	pakett,
	danat,
	klpd,
	satuan,
	pengadaan,
	pengadaan2,
	nilai,
	lokasi
) {
	$("#add-data-7").trigger("click");
	$("#id_7").val(id);
	$("#tpst_7").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_7").prop("disabled", true);
	$("#tahun_tender").val(tahunt);
	$("#paket_tender").val(pakett);
	$("#dana_tender").val(danat).trigger("change");
	$("#klpd_tender").val(klpd);
	$("#satuan_tender").val(satuan);
	if (pengadaan == "3") {
		$("#pengadaan_tender").val(3).trigger("change");
		$("#pengadaan_tender2").val(pengadaan2);
	} else {
		$("#pengadaan_tender").val(pengadaan).trigger("change");
	}
	$("#nilai_tender").val(nilai);
	$("#lokasi_tender").val(lokasi);
}

function editbarangjasa(
	id,
	tpst,
	tpst_desc,
	konstruksi,
	dokumen,
	penjelasan,
	penawaran,
	pembukaan,
	kualifikasi,
	pengumuman,
	sanggah,
	negosiasi,
	penetapan,
	pemenang,
	penujukan,
	penandatanganan
) {
	$("#add-data-8").trigger("click");
	$("#tpst_8").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_8").prop("disabled", true);
	$("#id_8").val(id);

	var variable = [
		konstruksi,
		dokumen,
		penjelasan,
		penawaran,
		pembukaan,
		kualifikasi,
		pengumuman,
		sanggah,
		negosiasi,
		penetapan,
		pemenang,
		penujukan,
		penandatanganan,
	];

	var label = [
		"konstruksi",
		"dokumen",
		"penjelasan",
		"penawaran",
		"pembukaan",
		"kualifikasi",
		"pengumuman",
		"sanggah",
		"negosiasi",
		"penetapan",
		"pemenang",
		"penunjukan",
		"penandatanganan",
	];

	for (var j = 0; j < variable.length; j++) {
		var a = variable[j].split(",");
		console.log(a);
		for (var k = 0; k < a.length; k++) {
			var lab = "#" + label[j] + (k + 1);
			console.log(a);
			$(lab).val(a[k]);
		}
	}
}

function editstatpelaksanaan(id, tpst, tpst_desc, stat_pelaksanaan) {
	$("#add-data-9").trigger("click");
	$("#tpst_9").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_9").prop("disabled", true);
	$("#id_9").val(id);
	$("#stat_pelaksanaan").val(stat_pelaksanaan).trigger("change");
}

function deleteData(id, table) {
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: "btn btn-success btn-sm swal2-styled-custom",
			cancelButton: "btn btn-danger btn-sm swal2-styled-custom",
		},
		buttonsStyling: false,
	});

	swalWithBootstrapButtons
		.fire({
			title: "Anda Yakin, hapus data ini?",
			text: "",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: '<i class="fas fa-check"></i> Ya',
			cancelButtonText: '<i class="fas fa-times"></i> Tidak',
			reverseButtons: true,
		})
		.then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					type: "post",
					dataType: "json",
					url: "deletecriteria",
					data: {
						id: id,
						table: table,
					},
					success: function (data) {
						Swal.fire({
							title: "Sukses!",
							text: "Hapus Data",
							icon: "success",
							showConfirmButton: false,
							timer: 1500,
						});
						loaddata(table);
					},
				});
			}
		});
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		var blah = input.id.replace("image", "blah");
		reader.onload = function (e) {
			$("#" + blah).attr("src", e.target.result);
			window.img = e.target.result;
		};
		reader.readAsDataURL(input.files[0]); // convert to base64 string
	}
}

function modaldetail(id, username, role, status, name, foto) {
	$("#modal-detail").modal({
		show: true,
	});

	$(".modal-title").html("Detail");

	var stt = "";
	if (status == 1) {
		stt += `<span class="badge badge-primary right">Aktif</span>`;
	} else {
		stt += `<span class="badge badge-warning right">Non Aktif</span>`;
	}

	$("#detail-foto").attr("src", foto);
	$("#detail-name").text(name);
	$("#detail-username").html("username: <i>" + username + "</i>");
	$("#detail-status").html(stt);
	$("#detail-role").text(role);
}
