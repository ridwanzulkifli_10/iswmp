$(function () {
	console.log("You are running jQuery version: " + $.fn.jquery);
	$(".summernote").summernote({
		height: 200, //set editable area's height
		codemirror: {
			// codemirror options
			theme: "monokai",
		},
		toolbar: {},
		disableResizeEditor: true,
		focus: true,
	});

	$(".note-editable").attr("name", "profile-input");

	$(".select2").select2();
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	$(".datatable").DataTable();

	//Date picker
	$("#reservationdate").datetimepicker({
		format: "L",
	});

	window.img = "";
	$("input[data-bootstrap-switch]").each(function () {
		// $(this).bootstrapSwitch('state', $(this).prop('checked'));
		$(this).bootstrapSwitch({
			onSwitchChange: function (e, state) {
				st = state;
			},
		});
	});

	$("#modal-default").on("show.bs.modal", function () {});

	$("#pasca > a").attr("class", "nav-link active");
	$("#pasca").attr("class", "nav-item menu-is-opening menu-open");
	$("#operasi > a").attr("class", "nav-link active");
	$("#operasi > a > i").addClass("text-info");

	$("#add-data-1").on("click", function () {
		$("#modal-1").modal({
			show: true,
		});
		$("#tpst_1").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_1");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_1").html(elem).trigger("change");
		$('[name="profil-input"]').val("");
		$("#kontrak_profil").val(0).trigger("change");
		$("#id_1").val("");
	});

	$("#add-data-2").on("click", function () {
		$("#modal-2").modal({
			show: true,
		});
		$("#tpst_2").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_2");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_2").html(elem).trigger("change");
		$('[name="kelembagaan-input"]').val(0).trigger("change");
		$("#id_2").val("");
	});

	$("#add-data-3").on("click", function () {
		$("#modal-3").modal({
			show: true,
		});
		$("#tpst_3").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_3");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_3").html(elem).trigger("change");
		$('[name="peraturan-input"]').val(0).trigger("change");
		$("#id_3").val("");
	});

	$("#add-data-4").on("click", function () {
		$("#modal-4").modal({
			show: true,
		});
		$("#tpst_4").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_4");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_4").html(elem).trigger("change");
		$('[name="pembiayaan-input"]').val(0).trigger("change");
		$("#id_4").val("");
	});

	$("#add-data-5").on("click", function () {
		$("#modal-5").modal({
			show: true,
		});
		$("#tpst_5").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_5");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_5").html(elem).trigger("change");
		$('[name="peran-input"]').val(0).trigger("change");
		$("#id_5").val("");
	});

	$("#add-data-6").on("click", function () {
		$("#modal-6").modal({
			show: true,
		});
		$("#tpst_6").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_6");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_6").html(elem).trigger("change");
		$('[name="operasional-input"]').val(0).trigger("change");
		$("#id_6").val("");
	});

	$("#add-data-7").on("click", function () {
		$("#modal-7").modal({
			show: true,
		});
		$("#tpst_7").prop("disabled", false);
		var local_tpst = window.localStorage.getItem("datatpst_7");
		let data_tpst = local_tpst.length ? JSON.parse(local_tpst) : [];
		var elem = '<option value="">...</option>';
		for (let index = 0; index < data_tpst.length; index++) {
			console.log(data_tpst[index]);
			elem += `<option value="${data_tpst[index]["id"]}">${data_tpst[index]["tpst"]}</option>`;
		}
		$("#tpst_7").html(elem).trigger("change");
		$('[name="uji-input"]').val(0).trigger("change");
		$("#id_7").val("");
	});

	$("#save-data-1").on("click", function () {
		savedata("data_profil_infrastruktur_terbangun");
	});

	$("#save-data-2").on("click", function () {
		savedata("data_aspek_kelembagaan");
	});

	$("#save-data-3").on("click", function () {
		savedata("data_aspek_peraturan");
	});

	$("#save-data-4").on("click", function () {
		savedata("data_aspek_pembiayaan");
	});

	$("#save-data-5").on("click", function () {
		savedata("data_aspek_peran_serta");
	});

	$("#save-data-6").on("click", function () {
		savedata("data_aspek_teknis_operasional");
	});

	loaddata("data_profil_infrastruktur_terbangun");
	loadoption("data_profil_infrastruktur_terbangun", 1);
	$("#custom-1").on("click", function () {
		loaddata("data_profil_infrastruktur_terbangun");
		loadoption("data_profil_infrastruktur_terbangun", 1);
	});
	$("#custom-2").on("click", function () {
		loaddata("data_aspek_kelembagaan");
		loadoption("data_aspek_kelembagaan", 2);
	});
	$("#custom-3").on("click", function () {
		loaddata("data_aspek_peraturan");
		loadoption("data_aspek_peraturan", 3);
	});
	$("#custom-4").on("click", function () {
		loaddata("data_aspek_pembiayaan");
		loadoption("data_aspek_pembiayaan", 4);
	});
	$("#custom-5").on("click", function () {
		loaddata("data_aspek_peran_serta");
		loadoption("data_aspek_peran_serta", 5);
	});
	$("#custom-6").on("click", function () {
		loaddata("data_aspek_teknis_operasional");
		loadoption("data_aspek_teknis_operasional", 6);
	});
});

function loadoption(param, kode) {
	$.ajax({
		type: "post",
		dataType: "json",
		url: "gettpst",
		data: {
			param: param,
		},
		success: function (result) {
			var res = result.data;
			var code = result.code;
			var item = code == 1 ? JSON.stringify(res) : [];

			window.localStorage.setItem("datatpst_" + kode, item);
			var elem = '<option value="">...</option>';
			if (result.code == 1) {
				for (let index = 0; index < res.length; index++) {
					elem += `<option value="${res[index]["id"]}">${res[index]["tpst"]}</option>`;
				}
			}

			$("#tpst_" + kode).html(elem);
			$("#tpst_" + kode).trigger("change");
		},
	});
}

function loaddata(param) {
	$.ajax({
		type: "post",
		dataType: "json",
		url: "getcriteria",
		data: {
			param: param,
		},
		success: function (result) {
			if (result.code == 1) {
				if (param == "data_profil_infrastruktur_terbangun") {
					var dt = $("#list_1").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "tahun_profil" },
							{ mDataProp: "paket_profil" },
							{ mDataProp: "satuan_profil" },
							{ mDataProp: "nilai_profil" },
							{ mDataProp: "lokasi_profil" },
							{ mDataProp: "kontrak_profil" },
							{ mDataProp: "tanggal_profil" },
							{ mDataProp: "koordinat_profil" },
							{ mDataProp: "lingkup_profil" },
							{ mDataProp: "kapasitas_profil" },
							{ mDataProp: "target_profil" },
							{ mDataProp: "realisasi_profil" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = ["-", "Lumpsum", "Harga Satuan", "Gabungan"];

									return $rowData[data];
								},
								aTargets: [7],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">	
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editprofile(${row.id},'${row.tpst}','${row.tpst_desc}','${row.tahun_profil}', '${row.paket_profil}', '${row.satuan_profil}', '${row.nilai_profil}', '${row.lokasi_profil}','${row.kontrak_profil}',  '${row.tanggal_profil}','${row.koordinat_profil}', '${row.lingkup_profil}', '${row.target_profil}', '${row.kapasitas_profil}','${row.realisasi_profil}' )"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_profil_infrastruktur_terbangun')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [14],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_aspek_kelembagaan") {
					var dt = $("#list_2").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "tugasfungsi" },
							{ mDataProp: "lembaga" },
							{ mDataProp: "struktur" },
							{ mDataProp: "kompetensi" },
							{ mDataProp: "pendampingan" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Ada peraturan terkait tugas dan fungsi institusi yang menangani sistem pengelolaan persampahan",
										"Masih dalam proses penyusunan peraturan terkait tugas dan fungsi institusi yang menangani sistem pengelolaan persampahan",
										"Tidak ada peraturan terkait tugas dan fungsi yang secara jelas menangani sistem pengelolaan persampahan",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Lembaga pengelola sudah terpisah dari regulator dalam bentuk PPK BLUD atau BUMD",
										"Lembaga pengelola sudah terpisah dari regulator dalam bentuk UPTD",
										"Lembaga pengelola belum terpisah antara regulator dan operator",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Tersedia struktur organisasi lembaga pengelola dan sesuai dengan kapasitas yang dibutuhkan",
										"Tersedia struktur organisasi lembaga pengelola tapi tidak sesuai dengan kapasitas yang dibutuhkan",
										"Tidak tersedia struktur organisasi lembaga pengelola",
									];

									return $rowData[data];
								},
								aTargets: [4],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"≥ 50% pegawai telah mendapatkan pelatihan atau diseminas atau workshop",
										"< 50% pegawai telah mendapatkan pelatihan atau diseminasi atau workshop",
										"Tidak ada pegawai yang mendapatkan pelatihan atau diseminasi atau workshop",
									];

									return $rowData[data];
								},
								aTargets: [5],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Sudah ada pendampingan operasional dan pemeliharaan infrastruktur persampahan (TPST) kepada operator secara rutin (periodik)",
										"Sudah ada pendampingan operasional dan pemeliharaan infrastruktur persampahan (TPST) kepada operator hanya pada saat serah terima infrastruktur saja",
										"Belum ada pendampingan operasional dan pemeliharaan infrastruktur persampahan (TPST) kepada operator",
									];

									return $rowData[data];
								},
								aTargets: [6],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editlembaga('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.tugasfungsi}', '${row.lembaga}', '${row.struktur}', '${row.kompetensi}', '${row.pendampingan}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_aspek_kelembagaan')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [7],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_aspek_peraturan") {
					var dt = $("#list_3").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "pengelolaan" },
							{ mDataProp: "retribusi" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Memiliki peraturan tentang sistem pengelolaan sampah",
										"Masih dalam proses penyusunan peraturan tentang sistem pengelolaan sampah",
										"Tidak memiliki peraturan yang terkait sistem pengelolaan air limbah domestik",
										"sistem pengelolaan persampahan",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Memiliki peraturan yang terkait retribusi pelayanan pengelolaan sampah",
										"Masih dalam proses penyusunan peraturan tentang retribusi pengelolaan sampah",
										"Tidak memiliki peraturan yang terkait retribusi pelayanan pengelolaan sampah",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editperaturan('${row.id}','${row.tpst}','${row.tpst_desc}','${row.pengelolaan}', '${row.retribusi}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_aspek_peraturan')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [4],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_aspek_pembiayaan") {
					var dt = $("#list_4").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "alokasi" },
							{ mDataProp: "capaian" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Ada alokasi anggaran dan mencukupi dengan biaya operasional dan pemeliharaan",
										"Ada alokasi anggaran tetapi tidak mencukupi dengan biaya operasional dan pemeliharaan",
										"Tidak ada alokasi anggaran biaya operasional dan pemeliharaan",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Capaian penerimaan retribusi persampahan > 75% dari jumlah KK atau penduduk yang terlayani",
										"Capaian penerimaan retribusi persampahan 50% sampai 75% dari jumlah KK atau penduduk yang terlayani",
										"Capaian penerimaan retribusi persampahan < 50% dari jumlah KK atau penduduk yang terlayani",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editbiaya('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.alokasi}', '${row.capaian}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_aspek_pembiayaan')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [4],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_aspek_peran_serta") {
					var dt = $("#list_5").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "masyarakat" },
							{ mDataProp: "swasta" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Ada peran aktif dari masyarakat terhadap pengelolaan persampahan secara rutin (dikelola sesuai perencanaan)",
										"Ada peran aktif dari masyarakat terhadap pengelolaan persampahan tapi tidak rutin (tidak terkelola sesuai perencanaan)",
										"Tidak ada peran aktif dari masyarakat terhadap pengelolaan persampaha",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Ada kerjasama dengan pihak swasta dalam pengelolaan sampah dan memiliki MOU",
										"Ada kerjasama dengan pihak swasta terhadap pengelolaan sampah tapi tidak memiliki MOU",
										"Tidak ada kerjasama dengan swasta terhadap pengelolaan sampah",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editperan('${row.id}','${row.tpst}','${row.tpst_desc}','${row.masyarakat}', '${row.swasta}', '${row.k3}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_aspek_peran_serta')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [4],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}

				if (param == "data_aspek_teknis_operasional") {
					var dt = $("#list_6").DataTable({
						destroy: true,
						paging: true,
						lengthChange: false,
						searching: true,
						ordering: true,
						info: true,
						autoWidth: false,
						responsive: false,
						pageLength: 10,
						aaData: result.data,
						aoColumns: [
							{ mDataProp: "id", width: "5%" },
							{ mDataProp: "tpst_desc" },
							{ mDataProp: "kondisi" },
							{ mDataProp: "kelengkapan" },
							{ mDataProp: "sop" },
							{ mDataProp: "laporankegiatan" },
							{ mDataProp: "bmn" },
							{ mDataProp: "id" },
						],
						order: [[0, "ASC"]],
						aoColumnDefs: [
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Kondisi bangunan dan fasilitas pendukung sesuai perencanaan dan berfungsi optimal",
										"Kondisi bangunan dan fasilitas pendukung sesuai perencanaan, tetapi tidak berfungsi optimal",
										"Kondisi bangunan dan fasilitas pendukung rusak sehingga tidak berfungsi",
									];

									return $rowData[data];
								},
								aTargets: [2],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Memiliki sarana dan prasarana sistem pengelolaan sampah yang lengkap dan sesuai dengan kriterian",
										"Memiliki sarana dan prasarana sistem pengelolaan ampah yang lengkap tetapi belum sesuai dengan kriteria",
										"Tidak memiliki sarana dan prasarana sistem pengelolaan sampah yang lengkap dan sesuai kriteria",
									];

									return $rowData[data];
								},
								aTargets: [3],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Memiliki SOP untuk operasional dan pemeliharaan yang sesuai dengan infrastruktur terbangun",
										"Memiliki SOP untuk operasional dan pemeliharaan tapi tidak sesuai dengan infrastruktur terbangun",
										"Tidak memiliki SOP untuk operasional dan pemeliharaan",
									];

									return $rowData[data];
								},
								aTargets: [4],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Memiliki laporan kegiatan operasi dan pemeliharaan secara rutin sesuai kondisi lapangan",
										"Memiliki laporan kegiatan operasi dan pemeliharaan tetapi tidak rutin",
										"Tidak memiliki laporan kegiatan operasi dan pemeliharaan",
									];

									return $rowData[data];
								},
								aTargets: [5],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = [
										"-",
										"Sudah serah terima aset BMN atau  sudah penghapusan aset kondisi rusak",
										"Dalam proses serah terima aset BMN atau proses penghapusan aset kondisi rusak",
										"Belum dilakukan proses serah terima aset BMN atau belum proses penghapusan aset kondisi rusak",
									];

									return $rowData[data];
								},
								aTargets: [6],
							},
							{
								mRender: function (data, type, row) {
									var $rowData = "";
									$rowData += `
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-info">Action</button>
                                    <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu">
                                      <a class="dropdown-item" href="#" onclick="editoperasional('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.kondisi}', '${row.kelengkapan}', '${row.sop}', '${row.laporankegiatan}', '${row.bmn}')"><i class="far fa-edit"></i> Edit</a>
                                      <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_aspek_teknis_operasional')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    </div>
                                  </div>`;

									return $rowData;
								},
								aTargets: [7],
							},
						],

						fnRowCallback: function (
							nRow,
							aData,
							iDisplayIndex,
							iDisplayIndexFull
						) {
							var index = iDisplayIndexFull + 1;
							$("td:eq(0)", nRow).html(" " + index);
							return;
						},

						fnInitComplete: function () {
							var that = this;
							var td;
							var tr;

							this.$("td").click(function () {
								td = this;
							});
							this.$("tr").click(function () {
								tr = this;
							});

							$("#listproj_filter input").bind("keyup", function (e) {
								return this.value;
							});
						},
					});
				}
			} else {
				var table = $("#list_1").DataTable();
				table.clear().draw();
			}
		},
	});
}

function savedata(param) {
	var formData = new FormData();
	if (param == "data_profil_infrastruktur_terbangun") {
		var profile = $('[name="profil-input"]');
		for (let i = 0; i < profile.length; i++) {
			var elem = profile[i];
			if (elem.id == "tpst_1") {
				elem.id = elem.id.replace("_1", "");
				formData.append("tpst_desc", $("#select2-tpst_1-container").text());
			}
			formData.append(elem.id, elem.value);
		}

		formData.append("table", param);
		if ($("#id_1").val()) {
			formData.append("id", $("#id_1").val());
		}
		if ($("#id_1").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Profile Profil Infrastruktur Terbangun";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Profile Profil Infrastruktur Terbangun";
		}
	}

	if (param == "data_aspek_kelembagaan") {
		var kelembagaan = $('[name="kelembagaan-input"]');
		for (let i = 0; i < kelembagaan.length; i++) {
			var elem = kelembagaan[i];
			if (elem.id == "tpst_2") {
				elem.id = elem.id.replace("_2", "");
				formData.append("tpst_desc", $("#select2-tpst_2-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_2").val()) {
			formData.append("id", $("#id_2").val());
		}
		if ($("#id_2").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Aspek Kelembagaan ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Aspek Kelembagaan ";
		}
	}

	if (param == "data_aspek_peraturan") {
		var peraturan = $('[name="peraturan-input"]');
		for (let i = 0; i < peraturan.length; i++) {
			var elem = peraturan[i];
			if (elem.id == "tpst_3") {
				elem.id = elem.id.replace("_3", "");
				formData.append("tpst_desc", $("#select2-tpst_3-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_3").val()) {
			formData.append("id", $("#id_3").val());
		}
		if ($("#id_3").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Aspek Peraturan";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Aspek Peraturan";
		}
	}

	if (param == "data_aspek_pembiayaan") {
		var pembiayaan = $('[name="pembiayaan-input"]');
		for (let i = 0; i < pembiayaan.length; i++) {
			var elem = pembiayaan[i];
			if (elem.id == "tpst_4") {
				elem.id = elem.id.replace("_4", "");
				formData.append("tpst_desc", $("#select2-tpst_4-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_4").val()) {
			formData.append("id", $("#id_4").val());
		}
		if ($("#id_4").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Aspek Pembiayaan";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Aspek Pembiayaan";
		}
	}

	if (param == "data_aspek_peran_serta") {
		var peran = $('[name="peran-input"]');
		for (let i = 0; i < peran.length; i++) {
			var elem = peran[i];
			if (elem.id == "tpst_5") {
				elem.id = elem.id.replace("_5", "");
				formData.append("tpst_desc", $("#select2-tpst_5-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_5").val()) {
			formData.append("id", $("#id_5").val());
		}
		if ($("#id_5").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Aspek Peran Serta Masyarakat/ Swasta ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Aspek Peran Serta Masyarakat/ Swasta ";
		}
	}

	if (param == "data_aspek_teknis_operasional") {
		var operasional = $('[name="operasional-input"]');
		for (let i = 0; i < operasional.length; i++) {
			var elem = operasional[i];
			if (elem.id == "tpst_6") {
				elem.id = elem.id.replace("_6", "");
				formData.append("tpst_desc", $("#select2-tpst_6-container").text());
			}
			formData.append(elem.id, elem.value);
		}
		formData.append("table", param);
		if ($("#id_6").val()) {
			formData.append("id", $("#id_6").val());
		}
		if ($("#id_6").val()) {
			var baseurl = "updateCriteria";
			var msg = "Update Data Aspek Teknis Operasional  ";
		} else {
			var baseurl = "saveCriteria";
			var msg = "Tambah Data Aspek Teknis Operasional  ";
		}
	}

	$.ajax({
		type: "post",
		url: baseurl,
		dataType: "json",
		cache: false,
		contentType: false,
		processData: false,
		data: formData,
		async: false,
		success: function (result) {
			Swal.fire({
				title: "Sukses!",
				text: msg,
				icon: "success",
				showConfirmButton: false,
				timer: 1500,
			});

			$(".modal").modal("hide");
			var kode = [
				"-",
				"data_profil_infrastruktur_terbangun",
				"data_aspek_kelembagaan",
				"data_aspek_peraturan",
				"data_aspek_pembiayaan",
				"data_aspek_peran_serta",
				"data_aspek_teknis_operasional",
			].indexOf(param);
			$('[data-select2-id="tpst_' + kode + '"]').attr("id", "tpst_" + kode);
			$("#custom-" + kode).trigger("click");
			// loaddata(param);
		},
	});
}

function editprofile(
	id,
	tpst,
	tpst_desc,
	tahun,
	paket,
	satuan,
	lokasi,
	nilai,
	kontrak,
	tanggal,
	koordinat,
	lingkup,
	target,
	kapasistas,
	realisasi
) {
	$("#add-data-1").trigger("click");
	$("#id_1").val(id);
	$("#tpst_1").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_1").prop(`disabled`, true);
	$("#tahun_profil").val(tahun);
	$("#paket_profil").val(paket);
	$("#satuan_profil").val(satuan);
	$("#kontrak_profil").val(kontrak).trigger("change");
	$("#nilai_profil").val(nilai);
	$("#lokasi_profil").val(lokasi);
	$("#tanggal_profil").val(tanggal);
	$("#koordinat_profil").val(koordinat);
	$("#lingkup_profil").val(lingkup);
	$("#kapasitas_profil").val(kapasistas);
	$("#target_profil").val(target);
	$("#realisasi_profil").val(realisasi);
}

function editlembaga(
	id,
	tpst,
	tpst_desc,
	tugasfungsi,
	lembaga,
	struktur,
	kompetensi,
	pendampingan
) {
	$("#add-data-2").trigger("click");
	$("#tpst_2").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_2").prop(`disabled`, true);
	$("#id_2").val(id);
	$("#tugasfungsi").val(tugasfungsi).trigger("change");
	$("#lembaga").val(lembaga).trigger("change");
	$("#struktur").val(struktur).trigger("change");
	$("#kompetensi").val(kompetensi).trigger("change");
	$("#pendampingan").val(pendampingan).trigger("change");
}

function editperaturan(id, tpst, tpst_desc, pengelolaan, retribusi) {
	$("#add-data-3").trigger("click");
	$("#tpst_3").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_3").prop(`disabled`, true);
	$("#id_3").val(id);
	$("#pengelolaan").val(pengelolaan).trigger("change");
	$("#retribusi").val(retribusi).trigger("change");
}
function editbiaya(id, tpst, tpst_desc, alokasi, capaian) {
	$("#add-data-4").trigger("click");
	$("#tpst_4").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_4").prop(`disabled`, true);
	$("#id_4").val(id);
	$("#alokasi").val(alokasi).trigger("change");
	$("#capaian").val(capaian).trigger("change");
}

function editperan(id, tpst, tpst_desc, masyarakat, swasta) {
	$("#add-data-5").trigger("click");
	$("#tpst_5").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_5").prop(`disabled`, true);
	$("#id_5").val(id);
	$("#masyarakat").val(masyarakat).trigger("change");
	$("#swasta").val(swasta).trigger("change");
}

function editoperasional(
	id,
	tpst,
	tpst_desc,
	kondisi,
	kelengkapan,
	sop,
	laporankegiatan,
	bmn
) {
	$("#add-data-6").trigger("click");
	$("#tpst_6").html(`<option value="${tpst}">${tpst_desc}</option>`);
	$("#tpst_6").prop(`disabled`, true);
	$("#id_6").val(id);
	$("#kondisi").val(kondisi).trigger("change");
	$("#kelengkapan").val(kelengkapan).trigger("change");
	$("#sop").val(sop).trigger("change");
	$("#laporankegiatan").val(laporankegiatan).trigger("change");
	$("#bmn").val(bmn).trigger("change");
}

function deleteData(id, table) {
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: "btn btn-success btn-sm swal2-styled-custom",
			cancelButton: "btn btn-danger btn-sm swal2-styled-custom",
		},
		buttonsStyling: false,
	});

	swalWithBootstrapButtons
		.fire({
			title: "Anda Yakin, hapus data ini?",
			text: "",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: '<i class="fas fa-check"></i> Ya',
			cancelButtonText: '<i class="fas fa-times"></i> Tidak',
			reverseButtons: true,
		})
		.then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					type: "post",
					dataType: "json",
					url: "deletecriteria",
					data: {
						id: id,
						table: table,
					},
					success: function (data) {
						Swal.fire({
							title: "Sukses!",
							text: "Hapus Data",
							icon: "success",
							showConfirmButton: false,
							timer: 1500,
						});
						loaddata(table);
					},
				});
			}
		});
}

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		var blah = input.id.replace("image", "blah");
		reader.onload = function (e) {
			$("#" + blah).attr("src", e.target.result);
			window.img = e.target.result;
		};
		reader.readAsDataURL(input.files[0]); // convert to base64 string
	}
}

function modaldetail(id, username, role, status, name, foto) {
	$("#modal-detail").modal({
		show: true,
	});

	$(".modal-title").html("Detail");

	var stt = "";
	if (status == 1) {
		stt += `<span class="badge badge-primary right">Aktif</span>`;
	} else {
		stt += `<span class="badge badge-warning right">Non Aktif</span>`;
	}

	$("#detail-foto").attr("src", foto);
	$("#detail-name").text(name);
	$("#detail-username").html("username: <i>" + username + "</i>");
	$("#detail-status").html(stt);
	$("#detail-role").text(role);
}
