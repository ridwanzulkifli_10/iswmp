$( document ).ready(function() {
  console.log('You are running jQuery version: ' + $.fn.jquery);
  
  $('#dashboard > a').attr('class','nav-link active');

  $('#tahun_chart').on('change', function (){
    window.location.href = `dashboard?tahun=${this.value}`;
  })
  
  var data_pengunjung = JSON.parse(chart_pengunjung)
  
  var thn = {}
  var sum = {}
  for (const tahun in data_pengunjung) {
      var datas = []
      var sum_datas = []
      data_pengunjung[tahun].forEach(element => {
        sum_datas.push(parseInt(element['total']))
        if(element['total'] == 0){
          element['total'] = null
        }
        datas.push(element['total'])
      });

    thn[tahun] = datas
    sum[tahun] = sum_datas.reduce((a, b) => a + b, 0)
  }

  var ser = []
  for (const th in thn) {
    ser.push({
      name: th,
      data: thn[th],
    })
    
  }

  // data_pengunjung.forEach(element => {
  //   if(element['total'] == 0){
  //     element['total'] = null
  //   }
  //   datas.push(element['total'])
  // });
  
  var options = {
    series: ser,
    chart: {
    height: 450,
    type: 'line',
    dropShadow: {
      enabled: true,
      color: '#000',
      top: 18,
      left: 7,
      blur: 10,
      opacity: 0.2
    },
    toolbar: {
      show: false
    }
  },
  // colors: ['#77B6EA', 'red'],
  dataLabels: {
    enabled: true,
  },
  stroke: {
    curve: 'smooth'
  },
  title: {
    text: 'Total Pengunjung Berdasarkan Tahun',
    align: 'left'
  },
  grid: {
    borderColor: '#e7e7e7',
    row: {
      colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
      opacity: 0.5
    },
  },
  markers: {
    size: 1
  },
  xaxis: {
    categories: ['Januari ', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'Desember'],
    title: {
      text: 'Bulan'
      // text: 'Tahun '+chart_tahun
    }
  },
  yaxis: {
    title: {
      text: 'Total Pengunjung'
    },
  },
  legend: {
    position: 'top',
    horizontalAlign: 'center',
    floating: true,
    offsetY: -25,
    offsetX: -5,
    formatter: function(seriesName, opts) {
      var lege = ''
      for (const key in sum) {
          if(key == seriesName){
            lege += '<b>Tahun: '+seriesName+' - Total: '+sum[key]+'</b>'
          }
      }
      return [lege]
    }
  },
  grid: {
    strokeDashArray: 4,
    xaxis: {
        lines: {
            show: true
        }
    }
},
  };

  var chart = new ApexCharts(document.querySelector("#chart"), options);
  chart.render();

})
