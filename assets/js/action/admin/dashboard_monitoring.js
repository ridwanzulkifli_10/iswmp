$( document ).ready(function() {
  console.log('You are running jQuery version: ' + $.fn.jquery);
  $('.table').DataTable();
  $('#dashboard-monitoring > a').attr('class','nav-link active');
  loaddata('data_status_usulan')
  loaddata('data_status_kegiatan')

  
})

function openTable(nomor) {
  $('[name="tahapan"]').hide()
  $(`#tahap_${nomor}`).show();
}

function loaddata(table){

  $.ajax({
      type: 'post',
      dataType: 'json',
      url: 'getcriteria',
      data : {
              param       : table,
              type        : '',
       },
      success: function(result){
        
        if(result.code == 1){
          if(table == 'data_status_usulan'){
            $('#panel_1').html(result.data.length)
            var dt = $("#list_1").DataTable({
              destroy: true,
              paging: true,
              lengthChange: true,
              searching: true,
              ordering: true,
              info: true,
              autoWidth: false,
              responsive: false,
              pageLength: 10,
              aaData: result.data,
              aoColumns: [
                { mDataProp: "id", width: "5%" },
                { mDataProp: "tpst_desc" },
                { mDataProp: "usulan" },
                { mDataProp: "id" },
              ],
              order: [[0, "ASC"]],
              aoColumnDefs: [
                {
                  mRender: function (data, type, row) {
                    var $rowData = [
                      "-",
                      "Siap dilaksanakan konstruksi",
                      "Bisa dilaksanakan namun perlu melengkapi RC",
                      "Belum bisa dilaksanakan konstruksi",
                    ];

                    return $rowData[data];
                  },
                  aTargets: [2],
                },
                {
                  mRender: function (data, type, row) {
                    var $rowData = "";
                    $rowData += `
                                      <div class="btn-group">
                                      <button type="button" class="btn btn-info">Action</button>
                                      <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                        <span class="sr-only">Toggle Dropdown</span>
                                      </button>
                                      <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item" href="#" onclick="editusulan('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.usulan}')"><i class="far fa-edit"></i> Edit</a>
                                        <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_status_usulan')"><i class="far fa-trash-alt"></i> Hapus</a>
                                      </div>
                                    </div>`;

                    return $rowData;
                  },
                  aTargets: [3],
                },
              ],

              fnRowCallback: function (
                nRow,
                aData,
                iDisplayIndex,
                iDisplayIndexFull
              ) {
                var index = iDisplayIndexFull + 1;
                $("td:eq(0)", nRow).html(" " + index);
                return;
              },

              fnInitComplete: function () {
                
                var column;
                var select = '<option value="">-Pilih TPST-</option>';

                this.api().columns(1).every( function () {
                   column = this;
                   column.data().unique().sort().each( function ( d, j ) {
                    select += '<option value="'+d+'">'+d+'</option>'
                  } );
                })

                $('div#list_1_wrapper div.row:first-child div:nth-child(1)').removeClass("col-sm-12 col-md-6").addClass("col-sm-12 col-md-2")
                $('div#list_1_wrapper div.row:first-child div:nth-child(2)').removeClass("col-sm-12 col-md-6").addClass("col-sm-12 col-md-3")
                $('div#list_1_wrapper div.row:first-child div:nth-child(2)').before(`<div class="col-sm-12 col-md-3">
                
                <select
									id="tpst_filter"
									name="usulan-input"
									class="form-control select2bs4"
								>
									${select}
								</select>
                </div>`);
                $('div#list_1_wrapper div.row:first-child div:nth-child(3)').before(`<div class="col-sm-12 col-md-3">
                <select
									id="usulan_filter"
									name="usulan-input"
									class="form-control select2bs4"
								>
									<option value="">-Pilih Status-</option>
									<option value="Siap dilaksanakan konstruksi">Siap dilaksanakan konstruksi</option>
									<option value="Bisa dilaksanakan namun perlu melengkapi RC">Bisa dilaksanakan namun perlu melengkapi RC</option>
									<option value="Belum bisa dilaksanakan konstruksi">Belum bisa dilaksanakan konstruksi</option>
								</select>
                </div>`);
              },
            });

            $('#tpst_filter').on('change', function () {
              dt.columns(1).search( this.value ).draw();
            });
            $('#usulan_filter').on('change', function () {
              dt.columns(2).search( this.value ).draw();
            });
          }
            
          
          if(table == 'data_status_kegiatan'){
            $('#panel_2').html(result.data.length)
            var dt = $("#list_2").DataTable({
              destroy: true,
              paging: true,
              lengthChange: true,
              searching: true,
              ordering: true,
              info: true,
              autoWidth: false,
              responsive: false,
              pageLength: 10,
              aaData: result.data,
              aoColumns: [
                { mDataProp: "id", width: "5%" },
                { mDataProp: "tpst_desc" },
                { mDataProp: "status_pelaksanaan" },
                { mDataProp: "id" },
              ],
              order: [[0, "ASC"]],
              aoColumnDefs: [
                {
                  mRender: function (data, type, row) {
                    var $rowData = [
                      "-",
                      "Pelaksanaan kegiatan berjalan baik",
                      "Pelaksanaan kegiatan berjalan baik, tetapi perlu perhatian",
                      "Pelaksanaan kegiatan tidak berjalan baik dan perlu perhatian khusus",
                    ];
  
                    return $rowData[data];
                  },
                  aTargets: [2],
                },
                {
                  mRender: function (data, type, row) {
                    var $rowData = "";
                    $rowData += `
                                      <div class="btn-group">
                                      <button type="button" class="btn btn-info">Action</button>
                                      <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                        <span class="sr-only">Toggle Dropdown</span>
                                      </button>
                                      <div class="dropdown-menu" role="menu">
                                        <a class="dropdown-item" href="#" onclick="editstatus('${row.id}','${row.tpst}','${row.tpst_desc}', '${row.status_pelaksanaan}')"><i class="far fa-edit"></i> Edit</a>
                                        <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, 'data_status_kegiatan')"><i class="far fa-trash-alt"></i> Hapus</a>
                                      </div>
                                    </div>`;
  
                    return $rowData;
                  },
                  aTargets: [3],
                },
              ],
  
              fnRowCallback: function (
                nRow,
                aData,
                iDisplayIndex,
                iDisplayIndexFull
              ) {
                var index = iDisplayIndexFull + 1;
                $("td:eq(0)", nRow).html(" " + index);
                return;
              },
  
              fnInitComplete: function () {
                
                var column;
                var select = '<option value="">-Pilih TPST-</option>';

                this.api().columns(1).every( function () {
                   column = this;
                   column.data().unique().sort().each( function ( d, j ) {
                    select += '<option value="'+d+'">'+d+'</option>'
                  } );
                })

                $('div#list_2_wrapper div.row:first-child div:nth-child(1)').removeClass("col-sm-12 col-md-6").addClass("col-sm-12 col-md-2")
                $('div#list_2_wrapper div.row:first-child div:nth-child(2)').removeClass("col-sm-12 col-md-6").addClass("col-sm-12 col-md-3")
                $('div#list_2_wrapper div.row:first-child div:nth-child(2)').before(`<div class="col-sm-12 col-md-3">
                <select
									id="tpst_2_filter"
									name="usulan-input"
									class="form-control select2bs4"
								>
									${select}
								</select>
                </div>`);
                $('div#list_2_wrapper div.row:first-child div:nth-child(3)').before(`<div class="col-sm-12 col-md-3">
                <select
									id="status_filter"
									name="usulan-input"
									class="form-control select2bs4"
								>
                <select
                id="status_filter"
                name="statuspelaksanaan-input"
                class="form-control select2bs4"
              >
                <option value="0">-Pilih-</option>
                <option value="Pelaksanaan kegiatan berjalan baik">Pelaksanaan kegiatan berjalan baik</option>
                <option value="Pelaksanaan kegiatan berjalan baik, tetapi perlu perhatian">
                  Pelaksanaan kegiatan berjalan baik, tetapi perlu perhatian
                </option>
                <option value="Pelaksanaan kegiatan tidak berjalan baik dan perlu perhatian khusus">
                  Pelaksanaan kegiatan tidak berjalan baik dan perlu perhatian khusus
                </option>
              </select>
								</select>
                </div>`);
              },
            });

            $('#tpst_2_filter').on('change', function () {
              dt.columns(1).search( this.value ).draw();
            });
            $('#status_filter').on('change', function () {
              dt.columns(2).search( this.value ).draw();
            });
          }
            
        }
      }
  })
}