$(function () {
  var tahun = parent.document.getElementById('lelang-tahun').value
  var bulan = parent.document.getElementById('bulan-target').value
  $('[name="title-tahun"]').html(tahun)

  loaddata(tahun, bulan)
});

function loaddata(tahun, bulan) {
  $.ajax({
    type: 'post',
    dataType: 'json',
    data: {
      tahun : tahun,
      bulan : bulan
    },
    url: 'getlelangrekap',
    success: function(result){
      var datapaket = result.data.paket
      var datagrafik = result.data.grafik

      function rupiah(val) {
        var rup = 0;
        var bilangan = val;
        var	number_string = bilangan.toString(),
            sisa 	= number_string.length % 3,
            rupiah 	= number_string.substr(0, sisa),
            ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
          
        if (ribuan) {
          separator = sisa ? '.' : '';
          rupiah += separator + ribuan.join('.');
          rup = 'Rp.'+rupiah;
        }
        return rup
      }

      $('#tot-hpspaket').html(rupiah(datapaket.hpspaket))
      $('#tot-hpspaketbelum').html(rupiah(datapaket.hpspaketbelum))
      $('#tot-hpspaketsudah').html(rupiah(datapaket.hpspaketsudah))
      $('#tot-hpspaketsudahkontrak').html(rupiah(datapaket.hpspaketsudahkontrak))

      $('#tot-pagupaket').html(rupiah(datapaket.pagupaket))
      $('#tot-pagupaketbelum').html(rupiah(datapaket.pagupaketbelum))
      $('#tot-pagupaketsudah').html(rupiah(datapaket.pagupaketsudah))
      $('#tot-pagupaketsudahkontrak').html(rupiah(datapaket.pagupaketsudahkontrak))

      $('#tot-paket').html(datapaket.paket)
      $('#tot-paketbelum').html(datapaket.paketbelum)
      $('#tot-paketsudah').html(datapaket.paketsudah)
      $('#tot-paketsudahkontrak').html(datapaket.paketsudahkontrak)
      
      var options = {
        series: [
          {
            name: 'Sudah Kontrak',
            data: datagrafik.kontrak
          },
          {
            name: 'Sudah Tayang',
            data: datagrafik.sudah
          },
          {
            name: 'Belum Tayang',
            data: datagrafik.belum,
          },
        ],
        chart: {
        type: 'bar',
        height: 350,
        stacked: true,
        toolbar: {
          show: false
        },
        zoom: {
          enabled: false
        }
      },
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            position: 'bottom',
            offsetX: -10,
            offsetY: 0
          }
        }
      }],
      // colors:['#ffc107', '#28a745'],
      plotOptions: {
        bar: {
          horizontal: false,
          borderRadius: 10,
          dataLabels: {
            total: {
              enabled: true,
              style: {
                fontSize: '13px',
                fontWeight: 900
              }
            }
          }
        },
      },
      xaxis: {
        categories: datagrafik.pelaksana,
      },
      legend: {
        position: 'right',
        offsetY: 40
      },
      fill: {
        opacity: 1
      }
      };
      
      var chart = new ApexCharts(document.querySelector("#chart"), options);
      chart.render();

    }
  })
}
