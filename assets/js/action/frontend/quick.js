console.log('You are running jQuery version: ' + $.fn.jquery);
$(function () {    
    window.rencana = []
    window.realisasi = []

    $('#tab-wrapper li:first').addClass('active');
    $('#tab-body > div').hide();
    $('#tab-body > div:first').show();
    $('#tab-wrapper a').click(function() {
        $('#tab-wrapper li').removeClass('active');
        $(this).parent().addClass('active');
        var activeTab = $(this).attr('href');
        $('#tab-body > div:visible').hide();
        $(activeTab).show();
        return false;
    });

    $('#ini-fisik').hide()
    // $('#quick').DataTable()
    loaddata('lelang', $('#lelang-tahun').val())
    loaddata('fisik', $('#fisik-tahun').val())
    loaddata('uang', $('#uang-tahun').val())
    var date = new Date()
    var year = date.getFullYear()
    var month = date.getMonth()
    var date = date.getDate()
    var monthText= ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desmber']
    var today = new Date(new Date().toLocaleString("en-US", {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: true
    }));
    var hour = 0
    
    if(today.getHours() >= 8 && today.getHours() < 12){
        hour = '08:00'
    } else if(today.getHours() > 12 && today.getHours() < 16){
        hour = '12:00'
    } else if(today.getHours() >= 16){
        hour = '16:00'
    } else if(today.getHours() < 8) {
        hour = '16:00'
    }
    var menit = (parseInt(today.getMinutes()) < 10) ? "0" + today.getMinutes() : today.getMinutes()
    $('#waktunya').html(date+' '+monthText[month]+' '+year+'; '+hour+' WIB')

    $('#lelang-tahun').on('change', function(){
        loaddata('lelang', this.value)
        var iframe = document.getElementById('frame-id');
        iframe.src = iframe.src;
    })

    $('#bulan-target').on('change', function(){
        loaddata('lelang', $('#lelang-tahun').val(), this.value)
    })

    $('#fisik-tahun').on('change', function(){
        loaddata('fisik', this.value)
        $('#tahun-kapan').html(this.value)
    })

    $('#modal-pin').modal('show');

    $('#pin').keypress(function (e) {
        if (e.which == 13) {
          $('#submit-pin').click();
          return false;    //<---- Add this line
        }
      });

      

})

function loaddata(param, tahun, bulan){
    if(param == 'fisik'){
        
        if(tahun == '2022'){
            var ren = []
            var rel = []
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'djck',
                data: {
                    tahun : tahun,
                },
                success: function(result){
                    
                    result.data.forEach(element => {
                        var kode = element.kode
                        var renk = []
                        var rr = []
                        var sum = 0
                        for (let index = 1; index <= 12; index++) {
                            var elrenk = parseInt(element['renk_b'+index]) / parseInt(element.pg) * 100;
                            var elrr = parseInt(element['rr_b'+index]) / parseInt(element.pg) * 100;
                            renk.push(elrenk.toFixed(2))
                            rr.push(elrr.toFixed(2))

                        }

                        ren[kode] = renk
                        rel[kode] = rr
                    });

                    var sum_pg = result.data.reduce((total, obj) => parseInt(obj.pg) + total,0)
                    var sum_renk_b1 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b1) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b2 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b2) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b3 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b3) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b4 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b4) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b5 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b5) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b6 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b6) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b7 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b7) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b8 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b8) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b9 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b9) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b10 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b10) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b11 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b11) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b12 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b12) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)

                    var sum_rr_b1 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b1) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b2 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b2) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b3 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b3) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b4 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b4) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b5 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b5) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b6 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b6) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b7 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b7) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b8 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b8) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b9 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b9) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b10 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b10) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b11 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b11) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b12 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b12) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    
                    var sum_renk = [
                            sum_renk_b1,
                            sum_renk_b2,
                            sum_renk_b3,
                            sum_renk_b4,
                            sum_renk_b5,
                            sum_renk_b6,
                            sum_renk_b7,
                            sum_renk_b8,
                            sum_renk_b9,
                            sum_renk_b10,
                            sum_renk_b11,
                            sum_renk_b12
                        ]

                    var sum_rr = [
                            sum_rr_b1,
                            sum_rr_b2,
                            sum_rr_b3,
                            sum_rr_b4,
                            sum_rr_b5,
                            sum_rr_b6,
                            sum_rr_b7,
                            sum_rr_b8,
                            sum_rr_b9,
                            sum_rr_b10,
                            sum_rr_b11,
                            sum_rr_b12
                        ]
                    
                    var options = {
                        series: [{
                            name: "Rencana",
                            data: sum_renk
                        },
                        {
                            name: "Realisasi",
                            data: sum_rr
                        }
                        ],
                        chart: {
                        height: 350,
                        type: 'line',
                        zoom: {
                        enabled: false
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'straight'
                    },
                    title: {
                        text: 'Kurva S Pekerjaan Fisik ISWMP',
                        align: 'center'
                    },
                    grid: {
                        row: {
                        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                        opacity: 0.5
                        },
                    },
                        xaxis: {
                            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mri', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
                        },
                        yaxis: {
                            max: 100
                        }
                    };
                   
                    $('#chart_luar_child').remove()
                    $('#chart_luar').html('<div id="chart_luar_child"></div>')
                    var chart = new ApexCharts(document.querySelector("#chart_luar_child"), options);
                    chart.render();

                    var keu = []
                    var fis = []
                    var dt = $('#quick').DataTable({
                        destroy: true,
                        paging: true,
                        lengthChange: false,
                        searching: true,
                        ordering: true,
                        info: true,
                        autoWidth: false,
                        responsive: false,
                        pageLength: 10,
                        aaData: result.data,
                        aoColumns: [
                            { 'mDataProp': 'kdsatker', 'width':'5%'},
                            { 'mDataProp': 'kdlokasi'},
                            { 'mDataProp': 'kode'},
                            { 'mDataProp': 'kdgiat'},
                            { 'mDataProp': 'kdoutput'},
                            { 'mDataProp': 'kdsoutput'},
                            { 'mDataProp': 'kdkmpnen'},
                            { 'mDataProp': 'pg'},
                            { 'mDataProp': 'rtot'},
                            { 'mDataProp': 'ufis'},
                            //   { 'mDataProp': 'pfis'},
                            { 'mDataProp': 'pg51'},
                            { 'mDataProp': 'pg52'},
                            { 'mDataProp': 'pg53'},
                            { 'mDataProp': 'pg57'},
                            { 'mDataProp': 'rr51'},
                            { 'mDataProp': 'rr52'},
                            { 'mDataProp': 'rr53'},
                            { 'mDataProp': 'rr_b1'},
                            { 'mDataProp': 'rr_b2'},
                            //   { 'mDataProp': 'rr_b3'},
                            //   { 'mDataProp': 'rr_b4'},
                        ],
                        order: [[0, 'ASC']],
                        aoColumnDefs:[
                            // {
                            //     target: 4,
                            //     visible: false,
                            //     searchable: false,
                            // },
                            { "bVisible": false, "aTargets": [16, 17] },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = ''
                                    if(row.kode == '05.631143.IA.4840.RBB.008.101.A'){
                                        $rowData = 'BALI';
                                    }else{
                                        $rowData = 'JAWA BARAT';
                                    }
                                    return $rowData;
                                },
                                aTargets: [1]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = '';
                                    switch (row.kode) {
                                        case '05.631124.IA.4840.RBB.008.101.A':
                                            $rowData = 'Pembangunan TPST RDF Kab. Kerawang dan Kab. Purwakarta'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.101.B':
                                            $rowData = 'Pembangunan TPST Skala Kawasan Kab. Kerawang dan Kab. Purwakarta'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.102.A':
                                            $rowData = 'Supervisi Pembangunan TPST RDF Kab. Kerawang dan Purwakarta'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.900.A':
                                            $rowData = 'Pengelolaan Kegiatan Pembangunan TPST RDF Kab. Kerawang dan Purwakarta'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.101.A':
                                            $rowData = 'Pembangunan TPST RDF Kota Bandung dan Kabupaten Bandung'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.102.A':
                                            $rowData = 'Supervisi Pembangunan TPST RDF Kota Bandung dan Kabupaten Bandung'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.900.A':
                                            $rowData = 'Administrasi Kegiatan Pembangunan TPST RDF Kota Bandung dan Kabupaten Bandung'
                                            break;
                                        case '05.631143.IA.4840.RBB.008.101.A':
                                            $rowData = 'Pembangunan TPST Kota Denpasar'
                                            break;
                                    }

                                    return $rowData;
                                },
                                aTargets: [3]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = ''
                                    switch (row.kode) {
                                        case '05.631124.IA.4840.RBB.008.101.A':
                                            $rowData = '1'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.101.B':
                                            $rowData = '1'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.102.A':
                                            $rowData = '1'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.900.A':
                                            $rowData = '1'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.101.A':
                                            $rowData = 'Pembangunan TPST RDF Kota Bandung dan Kabupaten Bandung'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.102.A':
                                            $rowData = '625'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.900.A':
                                            $rowData = '1'
                                            break;
                                        case '05.631143.IA.4840.RBB.008.101.A':
                                            $rowData = '182400'
                                            break;
                                        
                                    }
                                    return $rowData;
                                },
                                aTargets: [4]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = '';
                                    switch (row.kode) {
                                        case '05.631124.IA.4840.RBB.008.101.A':
                                            $rowData = 'Paket'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.101.B':
                                            $rowData = 'Paket'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.102.A':
                                            $rowData = 'Paket'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.900.A':
                                            $rowData = 'Laporan'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.101.A':
                                            $rowData = 'Kepala Keluarga'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.102.A':
                                            $rowData = 'Laporan'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.900.A':
                                            $rowData = 'Laporan'
                                            break;
                                        case '05.631143.IA.4840.RBB.008.101.A':
                                            $rowData = 'Kepala Keluarga'
                                            break;
                                    }
                                    return $rowData;
                                },
                                aTargets: [5]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = '';
                                    switch (row.kode) {
                                        case '05.631124.IA.4840.RBB.008.101.A':
                                            $rowData = 'Pekerjaan Konstruksi'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.101.B':
                                            $rowData = 'Pekerjaan Konstruksi'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.102.A':
                                            $rowData = 'Jasa Konsultansi'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.900.A':
                                            $rowData = 'AU'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.101.A':
                                            $rowData = 'Pekerjaan Konstruksi'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.102.A':
                                            $rowData = 'Jasa Konsultansi'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.900.A':
                                            $rowData = 'AU'
                                            break;
                                        case '05.631143.IA.4840.RBB.008.101.A':
                                            $rowData = 'Pekerjaan Konstruksi'
                                            break;
                                    }
                                    return $rowData;
                                },
                                aTargets: [6]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = '';
                                    switch (row.kode) {
                                        case '05.631124.IA.4840.RBB.008.101.A':
                                            $rowData = 'K'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.101.B':
                                            $rowData = 'K'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.102.A':
                                            $rowData = 'K'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.900.A':
                                            $rowData = 'AU'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.101.A':
                                            $rowData = 'K'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.102.A':
                                            $rowData = 'K'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.900.A':
                                            $rowData = 'AU'
                                            break;
                                        case '05.631143.IA.4840.RBB.008.101.A':
                                            $rowData = 'K'
                                            break;
                                    }
                                    return $rowData;
                                },
                                aTargets: [7]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = '';
                                    switch (row.kode) {
                                        case '05.631124.IA.4840.RBB.008.101.A':
                                            $rowData = 'MYC Lanjutan'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.101.B':
                                            $rowData = 'MYC Lanjutan'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.102.A':
                                            $rowData = 'MYC Lanjutan'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.900.A':
                                            $rowData = '-'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.101.A':
                                            $rowData = 'MYC Lanjutan'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.102.A':
                                            $rowData = 'MYC Lanjutan'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.900.A':
                                            $rowData = '-'
                                            break;
                                    }
                                    return $rowData;
                                },
                                aTargets: [8]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = 0;
                                    return $rowData;
                                },
                                aTargets: [9]
                            },
                            {
                                mRender: function (data, type, row){
                                    var	reverse = row.pg.toString().split('').reverse().join(''),
                                    ribuan 	= reverse.match(/\d{1,3}/g);
                                    ribuan	= ribuan.join('.').split('').reverse().join('');
                                    var $rowData = ribuan;
                                    return $rowData;
                                },
                                aTargets: [10, 11]
                            },
                            {
                                mRender: function (data, type, row){
                                    var	reverse = row.rtot.toString().split('').reverse().join(''),
                                    ribuan 	= reverse.match(/\d{1,3}/g);
                                    ribuan	= ribuan.join('.').split('').reverse().join('');
                                    var $rowData = ribuan;
                                    return $rowData;
                                },
                                aTargets: [12]
                            },
                            {
                                mRender: function (data, type, row){
                                    var	reverse = row.rtot.toString().split('').reverse().join(''),
                                    ribuan 	= reverse.match(/\d{1,3}/g);
                                    ribuan	= ribuan.join('.').split('').reverse().join('');
                                    var $rowData = ribuan;
                                    return $rowData;
                                },
                                aTargets: [13]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = (parseInt(row.rtot)/ parseInt(row.pg))*100;
                                    
                                    keu.push({keu:$rowData.toFixed(2)})
                                    return $rowData.toFixed(2);
                                },
                                aTargets: [14]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = (parseInt(row.ufis)/ parseInt(row.pg))*100;
                                    
                                    fis.push({fis:$rowData.toFixed(2)})
                                    return $rowData.toFixed(2);
                                },
                                aTargets: [15]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = 0;
                                    return $rowData;
                                },
                                aTargets: [16, 17]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $pkt = '';
                                    switch (row.kode) {
                                        case '05.631124.IA.4840.RBB.008.101.A':
                                            $pkt = 'Pembangunan TPST RDF Kab. Kerawang dan Kab. Purwakarta'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.101.B':
                                            $pkt = 'Pembangunan TPST Skala Kawasan Kab. Kerawang dan Kab. Purwakarta'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.102.A':
                                            $pkt = 'Supervisi Pembangunan TPST RDF Kab. Kerawang dan Purwakarta'
                                            break;
                                        case '05.631124.IA.4840.RBB.008.900.A':
                                            $pkt = 'Pengelolaan Kegiatan Pembangunan TPST RDF Kab. Kerawang dan Purwakarta'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.101.A':
                                            $pkt = 'Pembangunan TPST RDF Kota Bandung dan Kabupaten Bandung'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.102.A':
                                            $pkt = 'Supervisi Pembangunan TPST RDF Kota Bandung dan Kabupaten Bandung'
                                            break;
                                        case '05.631123.IA.4840.RBB.008.900.A':
                                            $pkt = 'Administrasi Kegiatan Pembangunan TPST RDF Kota Bandung dan Kabupaten Bandung'
                                            break;
                                        case '05.631143.IA.4840.RBB.008.101.A':
                                            $pkt = 'Pembangunan TPST Kota Denpasar'
                                            break;
                                    }

                                    var $rowData = `<button class="btn btn-info btn-md" onclick="opendata('${row.kode}', '${$pkt}')">Kurva S</button>`;
                                    return $rowData;
                                },
                                aTargets: [18]
                            },
                        ],
                        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                            var index = iDisplayIndexFull + 1;
                            $('td:eq(0)', nRow).html(' '+index);
                            return  ;
                        },

                        fnInitComplete: function () {
                            var api = this.api();
                            var intVal = function ( i ) {
                                return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '')*1 :
                                    typeof i === 'number' ?
                                        i : 0;
                            };

                            var count_fis = []
                            var tot_fis = api.column(14).data().reduce( function (a, b) {
                                    var totfis = intVal(a) + intVal(b)
                                    count_fis.push(totfis)
                                    return totfis;
                            }, 0 )
                            
                            // $('#total_fisik_foot').html(tot_fis)
                        }
                    });
                    var column2 = dt.column(2)
                    var column4 = dt.column(4)
                    var column5 = dt.column(5)
                    var column6 = dt.column(6)
                    var column7 = dt.column(7)
                    var column8 = dt.column(8)
                    var column9 = dt.column(9)
                    var column11 = dt.column(11)
                    var column13 = dt.column(13)
                    column2.visible(false)
                    column4.visible(false)
                    column5.visible(false)
                    column6.visible(false)
                    column7.visible(false)
                    column8.visible(false)
                    column9.visible(false)
                    column11.visible(false)
                    column13.visible(false)

                    
                    //   var uangan = parseFloat(dt.column(13).data().sum())/ result.data.length
                    //   var fisik = parseFloat(dt.column(14).data().sum())/ result.data.length

                    //   console.log(fisik);

                    var hash_keu = {};
                    var hash_fis = {};

                    keu.map(function(obj) {
                    return obj.keu;
                    }).filter(function(section){
                        if(!hash_keu[section]){
                            hash_keu[section] = section;
                            return true;
                        }

                        return false;
                    });
                    
                    fis.map(function(obj) {
                    return obj.fis;
                    }).filter(function(section){
                        if(!hash_fis[section]){
                            hash_fis[section] = section;
                            return true;
                        }

                        return false;
                    });

                    function sum( obj ) {
                        var sum = 0;
                        for( var el in obj ) {
                        if( obj.hasOwnProperty( el ) ) {
                            sum += parseFloat( obj[el] );
                        }
                        }
                        return sum;
                    }

                    var summed_keu = sum( hash_keu );
                    var summed_fis = sum( hash_fis );
                    var ini_sum_keu = (parseFloat(summed_keu/Object.keys(hash_keu).length)).toFixed(2)
                    var ini_sum_fis = (parseFloat(summed_fis/Object.keys(hash_fis).length)).toFixed(2)
                    $('#inisumkeu').html(ini_sum_keu)
                    $('#inisumfis').html(ini_sum_fis)
                    
                    $('#total_keuangan_foot').html(ini_sum_keu)
                    $('#total_fisik_foot').html(ini_sum_fis)
                }
            })
            
            window.rencana['data'] = ren
            window.realisasi['data'] = rel
        } else if(tahun == '2023'){
            var ren = []
            var rel = []
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'djck',
                data: {
                    tahun : tahun,
                },
                success: function(result){
                    
                    result.data.forEach(element => {
                        var kode = element.kode
                        var renk = []
                        var rr = []
                        var sum = 0
                        for (let index = 1; index <= 12; index++) {
                            var elrenk = parseInt(element['renk_b'+index]) / parseInt(element.pg) * 100;
                            var elrr = parseInt(element['rr_b'+index]) / parseInt(element.pg) * 100;
                            renk.push(elrenk.toFixed(2))
                            rr.push(elrr.toFixed(2))

                        }

                        ren[kode] = renk
                        rel[kode] = rr
                    });

                    var sum_pg = result.data.reduce((total, obj) => parseInt(obj.pg) + total,0)
                    
                    var sum_renk_b1 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b1) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b2 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b2) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b3 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b3) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b4 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b4) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b5 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b5) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b6 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b6) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b7 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b7) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b8 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b8) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b9 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b9) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b10 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b10) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b11 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b11) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_renk_b12 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.renk_b12) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)

                    var sum_rr_b1 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b1?obj.rr_b1:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b2 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b2?obj.rr_b2:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b3 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b3?obj.rr_b3:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b4 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b4?obj.rr_b4:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b5 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b5?obj.rr_b5:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b6 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b6?obj.rr_b6:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b7 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b7?obj.rr_b7:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b8 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b8?obj.rr_b8:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b9 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b9?obj.rr_b9:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b10 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b10?obj.rr_b10:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b11 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b11?obj.rr_b11:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)
                    var sum_rr_b12 = (parseInt(result.data.reduce((total, obj) => parseInt(obj.rr_b12?obj.rr_b12:0) + total,0)) / parseInt(sum_pg) * 100).toFixed(2)

                    var sum_renk = [
                            sum_renk_b1,
                            sum_renk_b2,
                            sum_renk_b3,
                            sum_renk_b4,
                            sum_renk_b5,
                            sum_renk_b6,
                            sum_renk_b7,
                            sum_renk_b8,
                            sum_renk_b9,
                            sum_renk_b10,
                            sum_renk_b11,
                            sum_renk_b12
                        ]

                    var sum_rr = [
                            sum_rr_b1,
                            sum_rr_b2,
                            sum_rr_b3,
                            sum_rr_b4,
                            sum_rr_b5,
                            sum_rr_b6,
                            sum_rr_b7,
                            sum_rr_b8,
                            sum_rr_b9,
                            sum_rr_b10,
                            sum_rr_b11,
                            sum_rr_b12
                        ]

                    var options = {
                        series: [{
                            name: "Rencana",
                            data: sum_renk
                        },
                        {
                            name: "Realisasi",
                            data: sum_rr
                        }
                        ],
                        chart: {
                        height: 350,
                        type: 'line',
                        zoom: {
                        enabled: false
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'straight'
                    },
                    title: {
                        text: 'Kurva S Pekerjaan Fisik ISWMP',
                        align: 'center'
                    },
                    grid: {
                        row: {
                        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                        opacity: 0.5
                        },
                    },
                        xaxis: {
                            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mri', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
                        },
                        yaxis: {
                            max: 100
                        }
                    };
                    
                    $('#chart_luar_child').remove()
                    $('#chart_luar').html('<div id="chart_luar_child"></div>')
                    var chart = new ApexCharts(document.querySelector("#chart_luar_child"), options);
                    chart.render();

                    var keu = []
                    var fis = []
                    var dt = $('#quick').DataTable({
                        destroy: true,
                        paging: true,
                        lengthChange: false,
                        searching: true,
                        ordering: true,
                        info: true,
                        autoWidth: false,
                        responsive: false,
                        pageLength: 10,
                        aaData: result.data,
                        aoColumns: [
                            { 'mDataProp': 'kdsatker', 'width':'5%'},
                            { 'mDataProp': 'nama_provinsi'},
                            { 'mDataProp': 'kode'},
                            { 'mDataProp': 'nama_paket'},
                            { 'mDataProp': 'kdoutput'},
                            { 'mDataProp': 'kdsoutput'},
                            { 'mDataProp': 'kdkmpnen'},
                            { 'mDataProp': 'pg'},
                            { 'mDataProp': 'rtot'},
                            { 'mDataProp': 'ufis'},
                            { 'mDataProp': 'pg51'},
                            { 'mDataProp': 'pg52'},
                            { 'mDataProp': 'pg53'},
                            { 'mDataProp': 'pg57'},
                            { 'mDataProp': 'rr51'},
                            { 'mDataProp': 'rr52'},
                            { 'mDataProp': 'rr53'},
                            { 'mDataProp': 'rr_b1'},
                            { 'mDataProp': 'rr_b2'},
                        ],
                        order: [[0, 'ASC']],
                        aoColumnDefs:[
                            { "bVisible": false, "aTargets": [16, 17] },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = 0;
                                    return $rowData;
                                },
                                aTargets: [9]
                            },
                            {
                                mRender: function (data, type, row){
                                    var	reverse = row.pg.toString().split('').reverse().join(''),
                                    ribuan 	= reverse.match(/\d{1,3}/g);
                                    ribuan	= ribuan.join('.').split('').reverse().join('');
                                    var $rowData = ribuan;
                                    return $rowData;
                                },
                                aTargets: [10, 11]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = 0
                                    if(row.rtot){
                                        var	reverse = row.rtot.toString().split('').reverse().join(''),
                                        ribuan 	= reverse.match(/\d{1,3}/g);
                                        ribuan	= ribuan.join('.').split('').reverse().join('');
                                        $rowData = ribuan;
                                    }
                                    return $rowData;
                                },
                                aTargets: [12]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = 0;
                                    if(row.rtot){
                                        var	reverse = row.rtot.toString().split('').reverse().join(''),
                                        ribuan 	= reverse.match(/\d{1,3}/g);
                                        ribuan	= ribuan.join('.').split('').reverse().join('');
                                        $rowData = ribuan;
                                    }
                                    return $rowData;
                                },
                                aTargets: [13]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = (parseInt(row.rtot?row.rtot:0)/ parseInt(row.pg))*100;
                                    
                                    keu.push({keu:$rowData.toFixed(2)})
                                    return $rowData.toFixed(2);
                                },
                                aTargets: [14]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = (parseInt(row.ufis)/ parseInt(row.pg))*100;
                                    fis.push({fis:$rowData.toFixed(2)})
                                    return $rowData.toFixed(2);
                                },
                                aTargets: [15]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = 0;
                                    return $rowData;
                                },
                                aTargets: [16, 17]
                            },
                            {
                                mRender: function (data, type, row){
                                    var $rowData = `<button class="btn btn-info btn-md" onclick="opendata('${row.kode}', '${row.nama_paket}')">Kurva S</button>`;
                                    return $rowData;
                                },
                                aTargets: [18]
                            },
                        ],
                        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                            var index = iDisplayIndexFull + 1;
                            $('td:eq(0)', nRow).html(' '+index);
                            return  ;
                        },

                        fnInitComplete: function () {
                            var api = this.api();
                            var intVal = function ( i ) {
                                return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '')*1 :
                                    typeof i === 'number' ?
                                        i : 0;
                            };

                            var count_fis = []
                            var tot_fis = api.column(14).data().reduce( function (a, b) {
                                    var totfis = intVal(a) + intVal(b)
                                    count_fis.push(totfis)
                                    return totfis;
                            }, 0 )
                            
                            // $('#total_fisik_foot').html(tot_fis)
                        }
                    });
                    var column2 = dt.column(2)
                    var column4 = dt.column(4)
                    var column5 = dt.column(5)
                    var column6 = dt.column(6)
                    var column7 = dt.column(7)
                    var column8 = dt.column(8)
                    var column9 = dt.column(9)
                    var column11 = dt.column(11)
                    var column13 = dt.column(13)
                    column2.visible(false)
                    column4.visible(false)
                    column5.visible(false)
                    column6.visible(false)
                    column7.visible(false)
                    column8.visible(false)
                    column9.visible(false)
                    column11.visible(false)
                    column13.visible(false)

                    
                    //   var uangan = parseFloat(dt.column(13).data().sum())/ result.data.length
                    //   var fisik = parseFloat(dt.column(14).data().sum())/ result.data.length

                    //   console.log(fisik);

                    var hash_keu = {};
                    var hash_fis = {};

                    keu.map(function(obj) {
                    return obj.keu;
                    }).filter(function(section){
                        if(!hash_keu[section]){
                            hash_keu[section] = section;
                            return true;
                        }

                        return false;
                    });
                    
                    fis.map(function(obj) {
                    return obj.fis;
                    }).filter(function(section){
                        if(!hash_fis[section]){
                            hash_fis[section] = section;
                            return true;
                        }

                        return false;
                    });

                    function sum( obj ) {
                        var sum = 0;
                        for( var el in obj ) {
                        if( obj.hasOwnProperty( el ) ) {
                            sum += parseFloat( obj[el] );
                        }
                        }
                        return sum;
                    }

                    var summed_keu = sum( hash_keu );
                    var summed_fis = sum( hash_fis );
                    var ini_sum_keu = (parseFloat(summed_keu/Object.keys(hash_keu).length)).toFixed(2)
                    var ini_sum_fis = (parseFloat(summed_fis/Object.keys(hash_fis).length)).toFixed(2)
                    $('#inisumkeu').html(ini_sum_keu)
                    $('#inisumfis').html(ini_sum_fis)
                    
                    $('#total_keuangan_foot').html(ini_sum_keu)
                    $('#total_fisik_foot').html(ini_sum_fis)
                }
            })

            window.rencana['data'] = ren
            window.realisasi['data'] = rel
        }else{
            $('#inisumkeu').html(0)
            $('#inisumfis').html(0)
            $('#total_keuangan_foot').html(0)
            $('#total_fisik_foot').html(0)
            $('#quick').DataTable().clear().draw();
            window.rencana['data'] = []
            window.realisasi['data'] = []
        }
        
    }

    if(param == 'lelang'){
        
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: 'getlelang',
            data: {
                tahun: tahun,
                bulan: bulan
            },
            success: function(result){
                $('#listlelang').DataTable().clear().draw();
                if(result.code == 1){
                        var dt = $('#listlelang').DataTable({
                        destroy: true,
                        paging: true,
                        lengthChange: false,
                        searching: true,
                        ordering: true,
                        info: true,
                        autoWidth: false,
                        responsive: false,
                        pageLength: 10,
                        aaData: result.data,
                            aoColumns: [
                                { 'mDataProp': 'id', 'width':'5%'},
                                { 'mDataProp': 'judul_paket'},
                                { 'mDataProp': 'metode_lelang'},
                                { 'mDataProp': 'pagu_hps'},
                                { 'mDataProp': 'hps'},
                                { 'mDataProp': 'pelaksana'},
                                { 'mDataProp': 'target_lelang'},
                                { 'mDataProp': 'tanggal_mulai'},
                                { 'mDataProp': 'tanggal_akhir'},
                                { 'mDataProp': 'status'},
                                { 'mDataProp': 'keterangan'},
                                { 'mDataProp': 'id'},
                            ],
                            order: [[0, 'ASC']],
                            aoColumnDefs:[
                                {
                                    mRender: function (data, type, row){
                                      if(type == 'display'){
                                        var $rowData = '';
                                        if(data){
                                          var bilangan = data;
                                          var	number_string = bilangan.toString(),
                                              sisa 	= number_string.length % 3,
                                              rupiah 	= number_string.substr(0, sisa),
                                              ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                                            
                                          if (ribuan) {
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                            $rowData = 'Rp.'+rupiah;
                                          }
                                        }
                                        return $rowData;
                                      }
                                      return data;
                                    },
                                    aTargets: [3,4]
                                  },
                                {
                                    mRender: function (data, type, row){
                                      if(type == 'display'){
                                        var $rowData = '';
                                        var bul = ['-', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                                        
                                        if(data !== null){
                                          $rowData =  bul[data] + ' ' + $('#lelang-tahun').val()
                                        }
                                        return $rowData;
                                      }
                                      return data;
                                    },
                                    aTargets: [6]
                                  },
                                  {
                                    mRender: function (data, type, row){
                                      if(type == 'display'){
                                        var $rowData = '';
                                        var isrow = '';
                                        var stat = '';
                                        var bul = ['-', 'Belum Tayang','Sudah Tayang','Sudah Kontrak']
                                        
                                        if(data !== null){
                                            $rowData =  typeof bul[data] != 'undefined' || bul[data] != null ? bul[data] : '-'
                                        }

                                        isrow += `<button class="btn btn-block btn-${data == 1 ? 'danger' : data == 2 ? 'warning' : data == 3 ? 'success' : '' }">${$rowData}</button>`

                                        var tgl_pengumuman = new Date(row.tanggal_mulai) //tanggal pengumuman
                                        var target_lelang = row.target_lelang //target lelang
                                        
                                        // tgl pengumuman > target lelang = terlambat (kotak merah)
                                        if(tgl_pengumuman.getMonth() + 1 > target_lelang){
                                            stat = `<button class="btn btn-block btn-danger">Terlambat</span>`
                                        }

                                        // tgl pengumuman = target lelang = tepat waktu (kotak hijau)
                                        if(tgl_pengumuman.getMonth() + 1 == target_lelang){
                                            stat = `<button class="btn btn-block btn-success">Tepat Waktu</button>`
                                        }

                                        isrow += stat

                                        return isrow
                                    }
                                      return data;
                                    },
                                    aTargets: [9]
                                  },
                                {
                                    mRender: function (data, type, row){
                                        if(data){
                                        var splt = data.split("/");
                                        var day = splt[1]
                                        var month = splt[0]
                                        var year = splt[2]

                                        var $rowData = '';
                                            $rowData += `${day}/${month}/${year}`;
                                        }else{
                                                $rowData = '-'
                                            }
                                        return $rowData;
                                    },
                                    aTargets: [7]
                                },
                                {
                                    mRender: function (data, type, row){
                                        if(data){
                                        var splt = data.split("/");
                                        var day = splt[1]
                                        var month = splt[0]
                                        var year = splt[2]

                                        var $rowData = '';
                                            $rowData += `${day}/${month}/${year}`;
                                        }else{
                                            $rowData = '-'
                                        }
                                        return $rowData;
                                    },
                                    aTargets: [8]
                                },
                                {
                                    mRender: function (data, type, row){
                                    
                                        var $rowData = '';
                                        if(typeof  row.files != 'undefined' && !row.link ){
                                            var path = row.files[0]['path']+'/'+row.files[0]['filename']
                                            $rowData = `<a type="button" class="btn btn-info" onclick="modallelang('${path}')"><i class="ion-ios-redo"></i> lihat jadwal</button>`;
                                        }else{
                                            if(row.link){
                                                $rowData = `<a type="button" class="btn btn-info" href="${row.link}" target="_blank"><i class="ion-ios-redo"></i> lihat jadwal</button>`;
                                            }
                                        }
                                            
                                        return $rowData;
                                    },
                                    aTargets: [11]
                                }
                            ],
        
                            fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                                var index = iDisplayIndexFull + 1;
                                $('td:eq(0)', nRow).html(' '+index);
                                return  ;
                            },
        
                            fnInitComplete: function () {
                                var that = this;
                                var td ;
                                var tr ;
        
                                this.$('td').click( function () {
                                    td = this;
                                });
                                this.$('tr').click( function () {
                                    tr = this;
                                });
        
        
                                $('#listproj_filter input').bind('keyup', function (e) {
                                    return this.value;
                                });
        
                            }
                        });
                }
    
            }
        });
    }

    if(param == 'uang'){
        
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: 'getglobal',
            data : {
                    param       : 'data_status_keuangan',
                    type        : 'status_keuangan',
            },
            success: function(result){
                var data = result.data
                var dp = []
                var act_dis = []
                var pv = []
                var act_pv = []
                for (let i = 0; i < data.length; i++) {
                    var element = data[i];
                    var par = element.param;
                    for (var e in element) {
                        if(
                            e != 'id' &&
                            e != 'param' &&
                            e != 'create_date' &&
                            e != 'update_date' &&
                            e != 'create_by' &&
                            e != 'update_by'
                        ){
                            if(par == 'dp'){
                                dp.push(element[e] ? element[e] : null)
                            }
                            if(par == 'act-dis'){
                                act_dis.push(element[e] ? element[e] : null)
                            }
                            if(par == 'pv'){
                                pv.push(element[e] ? element[e] : null)
                            }
                            if(par == 'act-pv'){
                                act_pv.push(element[e] ? element[e] : null)
                            }
                            if(par == 'stat' || par == 'act-stat'){
                                var st = ['-', 'Behind Schedule', 'At Risk', 'A Head On Schedule']
                                if(element[e] == 2){
                                    $(`#${par+'-'+e.replace('n','')}`).parent().css({"background": "red"})
                                }
                                if(element[e] == 1){
                                    $(`#${par+'-'+e.replace('n','')}`).parent().css({"background": "yellow"})
                                }
                                if(element[e] == 3){
                                    $(`#${par+'-'+e.replace('n','')}`).parent().css({"background": "green"})
                                }
                                element[e] = st[element[e]]
                                
                            }

                            $(`#${par+'-'+e.replace('n','')}`).text(element[e])
                        }
                    }
                    
                }
                
                var t_act_pv = []
                for (let i = 0; i < act_pv.length; i++) {
                    if(act_pv[i]){
                        t_act_pv.push(act_pv[i])
                    }
                }
                $('#iniactpv').html(t_act_pv.at(-1))
                var options = {
                    series: [
                        {
                            name: "Disbursement Plan",
                            data: dp
                        },
                        {
                            name: "Actual Disbursement",
                            data: act_dis
                        },
                        {
                            name: "PV Projection (PV)",
                            data: pv
                        },
                        {
                            name: "Actual PV",
                            data: act_pv
                        }
                    ],
                    chart: {
                        height: 350,
                        type: 'line',
                        zoom: {
                        enabled: false
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'straight'
                    },
                    title: {
                        text: '',
                        align: 'left'
                    },
                    grid: {
                        row: {
                        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                        opacity: 0.5
                        },
                    },
                    xaxis: {
                        categories: ['Q1-20','Q2-20','Q3-20','Q4-20','Q1-21','Q2-21','Q3-21','Q4-21','Q1-22','Q2-22','Q3-22','Q4-22','Q1-23','Q2-23','Q3-23','Q4-23','Q1-24','Q2-24','Q3-24','Q4-24','Q1-25','Q2-25','Q3-25'],
                    },
                    yaxis: {
                        type: "float"
                    },
                    colors:['#008000', '#f00', '#808080', '#ff0']
                };
          
                  
                  var chart = new ApexCharts(document.querySelector("#chart-uang"), options);
                  
                  chart.render();
      
            }
        });
    }

}

function kurva(datarencana, datarealisasi, pkt){
    console.log(datarencana);
    var options = {
        series: [{
            name: "Rencana",
            data: datarencana
        },
        {
            name: "Realisasi",
            data: datarealisasi
        }
        ],
        chart: {
            width: '70%',
            height: 350,
            type: 'line',
            zoom: {
            enabled: false
            }
        },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    title: {
        text: 'Kurva S '+pkt,
        align: 'center'
    },
    grid: {
        row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5
        },
    },
    xaxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mri', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
    },
    yaxis: {
        max: 100
    }
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
}

function opendata(kode, paket){
    var isRenc = window.rencana['data'][kode]
    var isReal = window.realisasi['data'][kode]
    $('#myModal').modal('show');
    kurva(isRenc, isReal, paket)
}

$('[name="ini-btn"]:first').trigger('click')

function viewtable(isthis, param){
    $('[name="ini-btn"]').attr('class', 'btn btn-info');
    $(isthis).removeClass();
    $(isthis).addClass('btn btn-primary');
    $('.ini-view').hide()
    $(`#${param}`).show()
    if(param == 'ini-fisik'){
        $('#chart_kontruksi').show()
    }else if(param == 'ini-lelang'){
        $('#chart_kontruksi').hide()
    }else{
        
    }
}

function modallelang(link){
    $('#modal-lelang').modal('show');
    $('#link-lelang').empty()
    $('#link-lelang').append(`<img class="img-center" src="${link}" />`)
    
    setTimeout(() => {
        $('.modal-dialog-lelang').css('width', $('#print_frame').width())
    }, 1000);
}

function checkpin() {
    if($('#pin').val() == 'iswmp2023'){
        $('#modal-pin').modal('hide');
        $('#pin').removeClass('pin-salah')
        $('section').css('filter', 'blur(0px)')
    }else{
        $('#pin').addClass('pin-salah')
    }
    
}

