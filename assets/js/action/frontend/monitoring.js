console.log('You are running jQuery version: ' + $.fn.jquery);
$(function () {    
    $('#tab-wrapper li:first').addClass('active');
    $('#tab-body > div').hide();
    $('#tab-body > div:first').show();
    $('#tab-wrapper a').click(function() {
        $('#tab-wrapper li').removeClass('active');
        $(this).parent().addClass('active');
        var activeTab = $(this).attr('href');
        $('#tab-body > div:visible').hide();
        $(activeTab).show();
        return false;
    });

    $('#rc').DataTable({
        aoColumnDefs:[
            {
                mRender: function (data, type, row){
                    var $rowData = '';
                    var src = '';
                    switch (row[2]) {
                        case 'Cicukang Oxbow':
                                src = 'https://drive.google.com/file/d/1rbR8b6XB_FQP8_DW_-EluzQRFb6hL89X/view?usp=sharing';
                            break;
                        case 'Citaliktik':
                                src = 'https://drive.google.com/file/d/14JqXYIBpdYdmow7pw_4zIy2ELLJUb-Uv/view?usp=sharing';
                            break;
                        case 'Ex TPA Babakan':
                                src = 'https://drive.google.com/file/d/1uGEqS2vuK__EgrNUAIcS_a23mul2FAKi/view?usp=sharing';
                            break;
                        case 'Jelekong':
                                src = 'https://drive.google.com/file/d/1gELp2_4jhInAuA9VOz3MAzrhVWrr7Cqr/view?usp=sharing';
                            break;
                        case 'RPH Baleendah':
                                src = 'https://drive.google.com/file/d/1uLkA4e4d28tOjPulnW1lEHnqUxnMu319/view?usp=sharing';
                            break;
                        case 'UPT BLK Disnaker':
                                src = 'https://drive.google.com/file/d/12Maz8IWd_Tv4gHJ8kv_rmRgfCMiK91_a/view?usp=sharing';
                            break;
                        case 'Wargamekar':
                                src = 'https://drive.google.com/file/d/1iy0epQlScc-ibO1eZeAmIexM7quGBrZO/view?usp=sharing';
                            break;
                        case 'Batujajar Timur':
                                src = 'https://drive.google.com/file/d/1MS0o3MGQTSyJMcIhZuWJ41wyqwz2iAED/view?usp=sharing';
                            break;
                        case 'Cilame':
                                src = 'https://drive.google.com/file/d/10RylfkOuyh61IitSY65m1VvAZ-kEZX5S/view?usp=sharing';
                            break;
                        case 'Desa Suntenjaya':
                                src = 'https://drive.google.com/file/d/1MsRZS9PSM0KNsQFNdmIEW3e4UUuuSByg/view?usp=sharing';
                            break;
                        case 'EX TPA Pasir Buluh':
                                src = 'https://drive.google.com/file/d/12inMckHQpzVqnv7bipkVlOhah4Bv-w-K/view?usp=sharing';
                            break;
                        case 'Komplek Pemda':
                                src = '';
                            break;
                        case 'Komplek Perumahan Kota Baru Parahyangan':
                                src = 'https://drive.google.com/file/d/1ZPYKHFcHr2X4vPyxRH4S1eY7cH0jP7ai/view?usp=sharing';
                            break;
                        case 'Bantarjaya':
                                src = 'https://drive.google.com/file/d/1f5_ZgXgnnyRIXnxy-1vVwzk29q_v6BPH/view?usp=sharing';
                            break;
                        case 'Kertamukti':
                                src = 'https://drive.google.com/file/d/1Ev7yTg_yw4rNaeamzw57vt7AWehfC7wC/view?usp=sharing';
                            break;
                        case 'Lenggahjaya':
                                src = 'https://drive.google.com/file/d/1ydd0mARigNPe2U47pfcCH1uUA-F6OSdz/view?usp=sharing';
                            break;
                        case 'Sindangjaya':
                                src = 'https://drive.google.com/file/d/17ljNaBkc3iaV9xgvJkD3pstANnkTo6_T/view?usp=sharing';
                            break;
                        case 'Kedungwaringin':
                                src = 'https://drive.google.com/file/d/19sVKt8RHhEh6HPMWExqNlU1yer1TcFf7/view?usp=sharing';
                            break;
                        case 'Bojong':
                                src = 'https://drive.google.com/file/d/1xiHr9mYKAG_89clcB8nSa1YL5RtZAAjl/view?usp=sharing';
                            break;
                        case 'Mekarsari':
                                src = 'https://drive.google.com/file/d/142AFWrL9UxjjHJ1vaDzp-0ijP42tjLN7/view?usp=sharing';
                            break;
                        case 'Bojongpicung':
                                src = 'https://drive.google.com/file/d/1FdsKkBKHsL4cMivgTjjVdy4Fvq61vWWK/view?usp=sharing';
                            break;
                        case 'Pasirembung':
                                src = 'https://drive.google.com/file/d/1kXPgl45AqfBOSG3z0uqxnY0gmFdPidGF/view?usp=sharing';
                            break;
                        case 'Cirejag':
                                src = '';
                            break;
                        case 'Jayakerta':
                                src = '';
                            break;
                        case 'Leuwisisir':
                                src = '';
                            break;
                        case 'Mekarjati':
                                src = '';
                            break;
                        case 'Cikolotok':
                                src = 'https://drive.google.com/file/d/1eLTLWEB0fNDHMutYiHG-C6Z7_zq50x7b/view?usp=sharing';
                            break;
                        case 'Nagrak':
                                src = 'https://drive.google.com/file/d/1E8XGxmcmcSd2ZiCJVgfbHT5X5-U-sAAq/view?usp=sharing';
                            break;
                        case 'Neglasari':
                                src = 'https://drive.google.com/file/d/14Ru-ZSRxp2BBXSgMrBoecJs5PyVlF3YZ/view?usp=sharing';
                            break;
                        case 'Sukatani':
                                src = 'https://drive.google.com/file/d/1BxGvaCWr7KKY2QFZmCRqY8yIP2hWQh_q/view?usp=sharing';
                            break;
                        case 'Tegalsari':
                                src = 'https://drive.google.com/file/d/1_biFM4qC2esunTcJE789acchJagJQXvn/view?usp=sharing';
                            break;
                        case 'EX-TPA Cicabe':
                                src = 'https://drive.google.com/file/d/1BASK6Rmgh9vXnEx-YhvvVMAKJT0Bo9yb/view?usp=sharing';
                            break;
                        case 'Nyengseret':
                                src = 'https://drive.google.com/file/d/14WbA2hG_-CO_bvqDVul3gTrLrlgRxoiV/view?usp=sharing';
                            break;
                        case 'Tegalega':
                                src = 'https://drive.google.com/file/d/1jKTf1ffzhXHWDhaL_5-bSx4kpS4-tu7P/view?usp=sharing';
                            break;
                        case 'Kolonel Masturi':
                                src = 'https://drive.google.com/file/d/1y7uLFDvahPu_txIIg3vzhPlSH8EKrfZV/view?usp=sharing';
                            break;
                        case 'Lebak Saat Cipageran':
                                src = 'https://drive.google.com/file/d/14Xj0XrCuoM0QXGCL-3X3MtCSlZZzgCpo/view?usp=sharing';
                            break;
                        case 'Kesiman Kertalangu':
                                src = 'https://drive.google.com/file/d/1jsgcvN6cEwrs1S3j5VM_UWARNN5T__gH/view?usp=sharing';
                            break;
                        case 'Padang Sambian':
                                src = 'https://drive.google.com/file/d/1OGKEHebOzfISPzem4vIqdZY2NftI8P7Z/view?usp=sharing';
                            break;
                        case 'Tahura':
                                src = 'https://drive.google.com/file/d/1fB6Pp_4mQNHRtGu_IpW6-gQ5MWm2ZmjM/view?usp=sharing';
                            break;
                    
                        default:
                            break;
                    }
                      $rowData += `
                          <a id="" href="${src}" target="_blank" class="btn btn-primary" src="" alt="">View</a>
                        `;
                    
                    return $rowData;
                },
                aTargets: [3]
            },
        ],
    });
})